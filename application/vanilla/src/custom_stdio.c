#include "custom_stdio.h"
#include "mmio.h"

#define PUTCHAR_ADDRESS MEMORY_ACCESS(STACK_START_ADDRESS + 0x0029)
#define GETCHAR_ADDRESS MEMORY_ACCESS(STACK_START_ADDRESS + 0x002A)
#define GETCHAR_ASSERT  MEMORY_ACCESS(STACK_START_ADDRESS + 0x002B)

void putchar(unsigned char c)
{
    PUTCHAR_ADDRESS = c;
}

void puts(const char * s)
{
	char * c = (char *) s;
	while (*c) putchar(*c++);
}

char * itoa(int value)
{
    // 12 bytes is big enough for an INT32.
    static char buffer[12];
    // Save original value.
    int original = value;
    int c = sizeof(buffer) - 1;
    // write trailing null in last byte of buffer.
    buffer[c] = 0;
    // If it's negative, note that and take the absolute value.
    if (value < 0)
    {
        value = -value;
    }
    // Write least significant digit of value that's left.
    do
    {
        buffer[--c] = (value % 10) + '0';    
        value /= 10;
    } while (value);
    if (original < 0)
    {
        buffer[--c] = '-';
    }
    return &buffer[c];
}

char getchar(void)
{
#if 1
    int c = -1;
    if (GETCHAR_ASSERT == 1) GETCHAR_ASSERT = 0;
    else GETCHAR_ASSERT = 1;
    while((c = GETCHAR_ADDRESS) == -1);
    return c;
#endif
    return 0;
}

void gets(char * string)
{
#if 1
    int position = 0;
    int c = -1;
    GETCHAR_ASSERT = 1;
    do
    {
        c = getchar();
        // Return Key
        if (c == '\n') break;
        // Backspace key
        if ((c == '\b') && (position > 0)) (--position);
        else string[position++] = (char) c;
    } while (position < 255);
    string[position] = '\0';
#endif
}