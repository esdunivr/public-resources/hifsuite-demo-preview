#include "watertank.h"



void firmware(void)
{

STATES status = ACTIVE;

uint16_t threshold;
bool high;
bool low;
bool open;
bool close;

while(1){
			
		switch(status){
			
			case ACTIVE:
				// set the initial value of the threshold 
				threshold = START_THRESHOLD;
				//retrieve data from the sensor
				watertank_sense(&high,&low);
				
				// Transition
				// if the water level is low, open the valve to fill the tank 
				if( low == true ) status = INCREASE;
				
			break;
			
			case INCREASE:
				// increase the threshold with open constant percentage (10%)
				threshold = threshold + ((uint16_t)(mul(threshold,OPEN_CONSTANT) >> 8));
				// saturator
				if(threshold > MAX_APERTURE) threshold = MAX_APERTURE;

				open  = true;
				close = false;
				// send to the actuator the new command and the new threshold
				watertank_actuate(open,close,threshold);
				status = NOTHING;
				
			break;
			
			case NOTHING:
				
				// Transitions
				if( low  == true ) status = INCREASE;
				if( high == true ) status = DECREASE;
				
			break;
			
			case DECREASE:
				
				//decrease the threshold of close constant (30%)
				threshold = threshold - ((uint16_t)(mul(threshold,CLOSE_CONSTANT) >> 8));
			
				open  = false;
				close = true;
				
				watertank_actuate(open,close,threshold);
				status = NOTHING;
				
			break;
			
		}	
		
		// Sense
		watertank_sense(&high,&low);
			
	}

}
