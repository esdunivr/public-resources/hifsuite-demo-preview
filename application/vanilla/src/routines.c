#include "routines.h"

//Multiplication Routine
uint32_t mul(uint16_t op1, uint16_t op2)
{
	// Prepare the variable used to read from pready.
	uint32_t result = 0x0000;
	// Prepare the data to send.
	set_pwdata_16(op1, op2);
	// Select the peripheral.
	set_psel(PSEL1);
	// Enable the operation.
	set_penable(1);
	// stay here untile prdata_rdy is ready.
	while (get_pready() == 0) __asm__("nop");
	// Get the result.
	result = get_prdata();
	// Disable the operation.
	set_penable(0);
	// Disable data stream from the bus.
	set_psel(NO_PSEL);
	//Clear Data
	//set_pwdata(0);
	// Return the result.
	return result;
}

void read_accelerometer(uint8_t * cap7, uint8_t * cap5, uint8_t * cap2, uint8_t * cap1)
{
	// Select the peripheral.
	set_psel(PSEL2);
	// Enable the operation.
	set_penable(1);
	// stay here untile prdata_rdy is ready.
	while (get_pready() == 0) __asm__("nop");
	// Get the result.
	get_prdata_8(cap7, cap5, cap2, cap1);
	// Disable the operation.
	set_penable(0);
	// Reset the peripheral.
	set_presetn(1);
	// Deactive the reset signal.
	set_presetn(0);
	// Disable data stream from the bus.
	set_psel(NO_PSEL);
}

void net_send_data(uint32_t data)
{
	// Set the mode of the network interface to SEND.
	set_pwrite(1);
	// Set the data to send.
	set_pwdata(data);
	// Select the peripheral.
	set_psel(PSEL3);
	// Enable the operation.
	set_penable(1);
	// stay here untile prdata_rdy is ready.
	while (get_pready() == 0) __asm__("nop");
	// Disable the operation.
	set_penable(0);
	// Clear the data.
	set_pwdata(0);
	// Clear the access mode.
	set_pwrite(0);
	// Reset the peripheral.
	set_presetn(1);
	// Disable data stream from the bus.
	set_psel(NO_PSEL);
	// Deactive the reset signal.
	set_presetn(0);
}

uint32_t net_receive_data(void)
{
	// Prepare the variable used to read from pready.
	uint32_t received_data;
	// Select the peripheral.
	set_psel(PSEL3);
	// Set the mode of the network interface to READ.
	set_pwrite(0);
	// Enable the operation.
	set_penable(1);
	// stay here untile prdata_rdy is ready.
	while (get_pready() == 0) __asm__("nop");
	// Get the result.
	received_data = get_prdata();
	// Clear the data.
	set_pwdata(0);
	// Disable the operation.
	set_penable(0);
	// Reset the peripheral.
	set_presetn(1);
	// Disable data stream from the bus.
	set_psel(NO_PSEL);
	// Deactive the reset signal.
	set_presetn(0);
	return received_data;
}

uint8_t get_random_number(void)
{
	uint32_t random_number;
	// Select the peripheral.
	set_psel(PSEL4);
	// Enable the operation.
	set_penable(1);
	// stay here untile prdata_rdy is ready.
	while (get_pready() == 0) __asm__("nop");
	// Get the result.
	random_number = get_prdata();
	// Disable the operation.
	set_penable(0);
	// Reset the peripheral.
	set_presetn(1);
	// Disable data stream from the bus.
	set_psel(NO_PSEL);
	// Deactive the reset signal.
	set_presetn(0);
	return random_number;
}


void watertank_sense(bool *high, bool *low)
{
	
	// Select the peripheral.
	set_psel(PSEL5);
	// Enable the operation.
	set_penable(1);
	// stay here untile prdata_rdy is ready.
	while (get_pready() == 0) __asm__("nop");	
	// Get High.
	*high = get_prdata_7_0();
	//Get Low
	*low = get_prdata_15_8();
	// Disable the operation.
	set_penable(0);
	// Reset the peripheral.
	set_psel(NO_PSEL);
}

void watertank_actuate(bool open, bool close, uint16_t threshold)
{
	//set the data in pwdata
	set_pwdata_7_0(open);
	set_pwdata_15_8(close);
	set_pwdata_31_16(threshold);
	// Select the peripheral.
	set_psel(PSEL6);
	// Enable the operation.
	set_penable(1);
	//wait until the operation is finished
	while (get_pready() == 0) __asm__("nop");
	//Disable Operation
	set_penable(0);
	// Reset the peripheral.
	set_psel(NO_PSEL);
	// Return the result.

}

void start_counter(uint16_t max_counter){

	uint8_t start_counting = 0x01;
	// Send the counter limit
	set_pwdata_15_0(max_counter);
	// Set Start counting operation
	//set_pwdata_7_0(0x00);
	//set_pwdata_15_8(0xff);
	//set_pwdata_8(0x00,0x10,start_counting,0x00);
	set_pwdata_23_16(start_counting);
	set_pwdata_31_24(0x00);

	// select the peripheral
	set_psel(PSEL7);
	//enable the operation
	set_penable(1);
	//Disable operation (The counter is still working)
	set_penable(0);
	// unselect the peripheral (The counter is still working)
	set_psel(NO_PSEL);
}

uint8_t end_counter(uint16_t * actual_counter){

	uint8_t stop_counting = 0x01;
	uint16_t counter;
	uint8_t counter_flag;
	// set Stop counting operation
	set_pwdata_8(0x00,0x00,0x00,stop_counting);

	// select the peripheral
	set_psel(PSEL7);
	//Enable the operation
	set_penable(1);

	//wait until the operation is finished
	while (get_pready() == 0) __asm__("nop");
	//retrieve the value of the actual counter
	*actual_counter = get_prdata_15_0();
	//retrieve the counter_flag 
	counter_flag = get_prdata_23_16();

	//Disable operation 
	set_penable(0);

	set_psel(NO_PSEL);

	return counter_flag;
}