//----------------------------------------------------------
//-- TITLE: Example of C main code rom for mos6502
//--
//-- Author: Stefano Centomo
//--
//-- Date:19/04/2018
//----------------------------------------------------------
//---------------------------------------
//---------------------------------------
//
//	M6502 
//	SUPPORTED TYPES & DIMENSIONS
//
//  int 		=>	16 bits	
//  char		=>	8 bits
//  uint8_t/int8_t	=>	8 bits
//  uint16_t/int16_t	=>	16 bits
//  uint32_t/int32_t	=>	32 bits
//  float/double	=> not supported
//	
//---------------------------------------
//---------------------------------------


#include <stdbool.h>

#include "custom_stdio.h"
#include "custom_stdlib.h"
#include "routines.h"

//#define TEST_CPU
#define TEST_MULTIPLIER
//#define TEST_ACCELEROMETER
//#define TEST_NETWORK
//#define TEST_IO
//#define TEST_LFSR
//#define WATERTANK
//#define TEST_WATCHDOG

#ifdef TEST_CPU
#include "m6502_check.h"
#endif
#ifdef WATERTANK
#include "watertank.h"
#endif

int main()
{
	// ------------------------------------------------------------------------
	// VARIABLES
	// ------------------------------------------------------------------------
#ifdef TEST_CPU
	bool cpu_test = true;
#endif
#ifdef TEST_MULTIPLIER
	uint16_t op1	= 5;
	uint16_t op2	= 2;
	uint32_t result	= 0;
#endif
#ifdef TEST_ACCELEROMETER
	uint8_t  cap7 	= 0;
	uint8_t  cap5 	= 0;
	uint8_t  cap2 	= 0;
	uint8_t  cap1 	= 0;
#endif
#ifdef TEST_NETWORK
	uint16_t network_in;
#endif
#ifdef TEST_IO
	char input[MAX_STRING_LENGTH];
#endif
#ifdef TEST_LFSR
	uint8_t random_number = 0;
#endif
#ifdef TEST_WATCHDOG
	uint16_t a =  	2 ;
	uint16_t b =	5 ;
	uint16_t c;
	uint16_t actual_counter = 0;
	uint16_t max_counter = 200;
	uint8_t counter_flag = 0 ;
uint8_t data = (max_counter & 0xff00) >> 8;
#endif
	// ------------------------------------------------------------------------
	// FUNCTIONALITY
	// ------------------------------------------------------------------------
	puts("Starting boot sequence.\n");

	// ------------------------------------------------------------------------
#ifdef TEST_CPU
	cpu_test = cpu_test & cpu_test_1();
	cpu_test = cpu_test & cpu_test_2();
	cpu_test = cpu_test & cpu_test_3();
	cpu_test = cpu_test & cpu_test_4();
	cpu_test = cpu_test & cpu_test_5();
	cpu_test = cpu_test & cpu_test_6();
 	puts("CPU test outcome : ");
 	//putchar(cpu_test);
 	putchar('\n');
#endif

	// ------------------------------------------------------------------------
	// Send op1 and op2 to the multiplier. through the bus.
	// return the result into result variable, passed as reference.
	// the routine is defined into routines.h routines.c
#ifdef TEST_MULTIPLIER
	puts("Multiplying ");
 	puts(itoa(op1));
 	puts(" for ");
 	puts(itoa(op2));
 	puts("...\n");
 	result = mul(op1, op2);
 	puts("The result of the multiplication is : ");
 	puts(itoa(result));
 	putchar('\n');
#endif

	// ------------------------------------------------------------------------
#ifdef TEST_ACCELEROMETER
 	read_accelerometer(&cap7, &cap5, &cap2, &cap1);
 	puts("Accelerometer data:\n");
 	puts("\tCAP 7 = ");
 	puts(itoa(cap7));
 	puts(";\n");
 	puts("\tCAP 5 = ");
 	puts(itoa(cap5));
 	puts(";\n");
 	puts("\tCAP 2 = ");
 	puts(itoa(cap2));
 	puts(";\n");
 	puts("\tCAP 1 = ");
 	puts(itoa(cap1));
 	puts(";\n");
#endif

	// ------------------------------------------------------------------------
#ifdef TEST_NETWORK
 	puts("Sending the answer '42' via network...\n");
 	net_send_data(42);
 	puts("Receiving result...");
 	network_in = net_receive_data();
 	puts(" done :");
 	puts(itoa(network_in));
 	putchar('\n');
#endif

	// ------------------------------------------------------------------------
#ifdef TEST_IO
	puts("Input   : ");
	gets(input);
	puts("Received: ");
	puts(input);
 	putchar('\n');
#endif

	// ------------------------------------------------------------------------
#ifdef TEST_LFSR
 	while(true)
 	{
		//random_number = get_random_number();
		random_number = rand();
		puts("Random Number : ");
 		puts(itoa(random_number));
 		putchar('\n');
 	}
#endif

	// ------------------------------------------------------------------------
#ifdef TEST_WATCHDOG
 	
	start_counter(max_counter);
 	counter_flag = end_counter(&actual_counter);

 	putchar('\n');
 	puts("Max    Counter : ");
 	puts(itoa(max_counter));
 	putchar('\n');
 	puts("Actual Counter : ");
 	puts(itoa(actual_counter));
 	putchar('\n');
 	puts("Counter Flag   : ");
 	puts(itoa(counter_flag));
 	putchar('\n');

	start_counter(max_counter);
	a= b+c;
 	counter_flag = end_counter(&actual_counter);


 	putchar('\n');
 	puts("Max    Counter : ");
 	puts(itoa(max_counter));
 	putchar('\n');
 	puts("Actual Counter : ");
 	puts(itoa(actual_counter));
 	putchar('\n');
 	puts("Counter Flag   : ");
 	puts(itoa(counter_flag));
 	putchar('\n');

	start_counter(max_counter);
	a= b+c;
	b= c+a;
	c= b+a;
 	counter_flag = end_counter(&actual_counter);

 	putchar('\n');
 	puts("Max    Counter : ");
 	puts(itoa(max_counter));
 	putchar('\n');
 	puts("Actual Counter : ");
 	puts(itoa(actual_counter));
 	putchar('\n');
 	puts("Counter Flag   : ");
 	puts(itoa(counter_flag));
 	putchar('\n');

 	start_counter(max_counter);
 	counter_flag = end_counter(&actual_counter);
 	
 	putchar('\n');
 	puts("Max    Counter : ");
 	puts(itoa(max_counter));
 	putchar('\n');
 	puts("Actual Counter : ");
 	puts(itoa(actual_counter));
 	putchar('\n');
 	puts("Counter Flag   : ");
 	puts(itoa(counter_flag));
 	putchar('\n');

#endif
	// ------------------------------------------------------------------------
#ifdef WATERTANK
		firmware();
#endif

	puts("Kernel execution completed.\n");

	stop_execution();
	return 0;
}
