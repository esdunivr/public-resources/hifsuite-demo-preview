#ifndef CUSTOM_STDIO
#define CUSTOM_STDIO

#include "defines.h"

void putchar(char c);

void puts(const char * s);

char * itoa(int value);

char getchar(void);

void gets(char * string);

#endif