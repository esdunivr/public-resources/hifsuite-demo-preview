#include <limits.h>
#include <stdint.h>
#include <stdbool.h>

// ----------------------------------------------------------------------------
// ALU AND CPU TEST 1
// ----------------------------------------------------------------------------
bool cpu_test_1()
{
    uint8_t data[] = {3, 2, 10, 56, 23, 0};
    uint8_t expected[] = {94, 0, 46, 130, 88, 141};
    uint8_t len = 6;
    uint8_t result = 0;
    uint8_t stage = 0;
    uint8_t i = 0;
    // ------------------------------------------------------------------------
    // STAGE 0 - ADD
    for (i = 0; i < len; i++)
    {
        result += data[i];
    }
    if (result != expected[stage++]) return false;

    // ------------------------------------------------------------------------
    // STAGE 1 - SUB
    for (i = 0; i < len; i++)
    {
        result -= data[i];
    }
    if (result != expected[stage++]) return false;
    // ------------------------------------------------------------------------
    // STAGE 2 - DIV
    for (i = 0; i < len; i++)
    {
        result += (data[i] / 2);
    }
    if (result != expected[stage++]) return false;
    // ------------------------------------------------------------------------
    // STAGE 3 - MUL
    for (i = 0; i < len; i++)
    {
        if ((data[i] % 2) == 0) result += (data[i] * 2);
        else result -= (data[i] * 2);
    }
    if (result != expected[stage++]) return false;
    // ------------------------------------------------------------------------
    // STAGE 4 - SHIFT&DIVIDE
    for (i = 0; i < len; i++)
    {
        if ((data[i] % 2) == 0) result -= ((data[i] << 1) / 2);
        else result += ((data[i] << 1) / 2);
    }
    if (result != expected[stage++]) return false;
    // ------------------------------------------------------------------------
    // STAGE 5 - POW
    for (i = 0; i < len; i++)
    {
        if ((data[i] % 2) == 0) result += (data[i] ^ i);
        else result -= (data[i] ^ i);
    }
    if (result != expected[stage++]) return false;
    return true;
}

// ----------------------------------------------------------------------------
// ALU AND CPU TEST 2
// ----------------------------------------------------------------------------
bool cpu_test_2()
{
    unsigned char failures = 0;
    unsigned int aint0 = 0;
    unsigned int aint1 = 0;
    unsigned char achar0 = 0;
    unsigned char achar1 = 0;
    unsigned char achar2 = 0;
    unsigned char achar3 = 0;
    unsigned char * acharP = 0;
    // ------------------------------------------------------------------------
    achar0 = achar0 + 5;
    if (achar0 != 5)
        failures++;
    achar0 += 10;
    if (achar0 != 15)
        failures++;
    achar0 = achar0 + 1;  /*Should be an increment */
    if (achar0 != 16)
        failures++;
    for (achar1 = 0; achar1 < 100; achar1++)
        achar0 += 2;
    if (achar0 != 216)
        failures++;
    // ------------------------------------------------------------------------
    achar0 = 16;
    achar1 = 0;
    achar1 = achar1 + achar0;
    if (achar1 != 16)
        failures++;
    for (achar2 = 0; achar2 < 7; achar2++)
        achar1 += achar0;
    if (achar1 != 128)
        failures++;
    // ------------------------------------------------------------------------
    achar0 = 0;
    achar1 = 32;
    achar0++;
    achar0 = achar0 + 1;
    achar0 = achar0 + 2;
    achar0 = achar0 + 3;
    if (achar0 != 7)
        failures++;
    achar1 += achar0;
    if (achar1 != 39)
        failures++;
    achar2 = achar1 + achar0;
    if (achar2 != 46)
        failures++;
    achar3 = achar2 + achar1 + achar0;
    if (achar3 != 92)
        failures++;
    // ------------------------------------------------------------------------
    return (failures == 0);
}

// ----------------------------------------------------------------------------
// ALU AND CPU TEST 3
// ----------------------------------------------------------------------------
bool cpu_test_3()
{
    unsigned char failures = 0;
    unsigned int aint0 = 0;
    unsigned int aint1 = 0;
    unsigned int aint2 = 0;
    unsigned int aint3 = 0;
    // ------------------------------------------------------------------------
    aint0 = 16;
    aint1 = 0;
    aint1 = aint1 + aint0;
    if (aint1 != 16)
        failures++;
    for (aint2 = 0; aint2 < 7; aint2++)
        aint1 += aint0;
    if (aint1 != 128)
        failures++;
    // ------------------------------------------------------------------------
    aint0 = 0;
    aint1 = 32;
    aint2 = 0;
    aint0++;
    aint0 = aint0 + 1;
    aint0 = aint0 + 2;
    aint0 = aint0 + 3;
    if (aint0 != 7)
        failures++;
    aint1 += aint0;
    if (aint1 != 0x27)
        failures++;
    aint2 = aint1 + aint0;
    if (aint2 != 0x2e)
        failures++;
    aint3 = aint2 + aint1 + aint0;
    if (aint3 != 0x5c)
        failures++;
    aint3 += 0xa0;
    if (aint3 != 0xfc)
        failures++;
    aint3 += aint0;
    if (aint3 != 0x103)
        failures++;
    aint1 += 0xffc0;
    if (aint1 != 0xffe7)
        failures++;
    aint3 = aint2 + aint1 + aint0;
    if (aint3 != 0x1c)
        failures++;
    // ------------------------------------------------------------------------
    aint0 = 0;
    aint0 += 0x0001;
    if (aint0 != 1)
        failures++;
    aint0 += 0x00;
    if (aint0 != 1)
        failures++;
    aint0 += 0x00fe;
    if (aint0 != 0x00ff)
        failures++;
    aint0 += 0x0001;
    if (aint0 != 0x0100)
        failures++;
    aint0++;
    if (aint0 != 0x0101)
        failures++;
    aint0 += 0x00ff;
    if (aint0 != 0x0200)
        failures++;
    aint0 += 0x00a0;
    if (aint0 != 0x02a0)
        failures++;
    aint0 += 0x0061;
    if (aint0 != 0x0301)
        failures++;
    aint0 += 0x0100;
    if (aint0 != 0x0401)
        failures++;
    aint0 += 0x0101;
    if (aint0 != 0x0502)
        failures++;
    aint0 += 0x00fd;
    if (aint0 != 0x05ff)
        failures++;
    aint0 += 0x0101;
    if (aint0 != 0x0700)
        failures++;
    aint0 += 0x01ff;
    if (aint0 != 0x08ff)
        failures++;
    aint0 += 0x01ff;
    if (aint0 != 0x0afe)
        failures++;
    aint0 += 0xff02;
    if (aint0 != 0x0a00)
        failures++;
    aint0 += 0xffff;
    if (aint0 != 0x09ff)
        failures++;
    aint0 += 0xff01;
    if (aint0 != 0x0900)
        failures++;
    aint0 += 0xff00;
    if (aint0 != 0x0800)
        failures++;
    aint0 += 0xff01;
    if (aint0 != 0x0701)
        failures++;
    aint0 += 0x0300;
    if (aint0 != 0x0a01)
        failures++;
    aint0 += 0x03ff;
    if (aint0 != 0x0e00)
        failures++;
    aint0 += 0x0301;
    if (aint0 != 0x1101)
        failures++;
    aint0 += 0x03fe;
    if (aint0 != 0x14ff)
        failures++;
    aint0 += 0x0301;
    if (aint0 != 0x1800)
        failures++;
    // ------------------------------------------------------------------------
    return (failures == 0);
}

// ----------------------------------------------------------------------------
// ALU AND CPU TEST 4
// ----------------------------------------------------------------------------
bool cpu_test_4()
{
    unsigned char failures = 0;
    unsigned int aint0 = 0;
    unsigned int aint1 = 0;
    unsigned char achar0 = 0;
    unsigned char achar1 = 0;
    unsigned char achar2 = 0;
    unsigned char achar3 = 0;
    unsigned char * acharP = 0;
    // ------------------------------------------------------------------------
    achar0 = achar0 - 5;
    if (achar0 != 0xfb)
        failures++;
    achar0 -= 10;
    if (achar0 != 0xf1)
        failures++;
    achar0 = achar0 - 1;
    if (achar0 != 0xf0)
        failures++;
    for (achar1 = 0; achar1 < 100; achar1++)
        achar0 -= 2;
    if (achar0 != 40)
        failures++;

    // ------------------------------------------------------------------------
    achar0 = 1;
    achar1 = 100;
    achar1 = achar1 - achar0;
    if (achar1 != 99)
        failures++;
    for (achar2 = 0; achar2 < 7; achar2++)
        achar1 -= achar0;
    if (achar1 != 92)
        failures++;

    // ------------------------------------------------------------------------
    achar0 = 10;
    achar1 = 32;
    achar0--;
    achar0 = achar0 - 1;
    achar0 = achar0 - 2;
    achar0 = achar0 - 3;
    if (achar0 != 3)
        failures++;
    achar1 -= achar0;
    if (achar1 != 29)
        failures++;
    achar2 = achar1 - achar0;
    if (achar2 != 26)
        failures++;
    achar3 = achar2 - achar1 - achar0;
    if (achar3 != 0xfa)
        failures++;

    // ------------------------------------------------------------------------
    aint0 = 0;
    achar0 = 0;
    achar0 = 2 - achar0;
    if (achar0 != 2)
        failures++;
    aint0 = 2 - aint0;
    if (aint0 != 2)
        failures++;
    aint0--;
    if (aint0 != 1)
        failures++;
    aint0 = 0x100 - aint0;
    if (aint0 != 0xff)
        failures++;
    aint0 = 0xff00 - aint0;
    if (aint0 != 0xfe01)
        failures++;
    aint0 = 0x0e01 - aint0;
    if (aint0 != 0x1000)
        failures++;
    aint0 = 0x10ff - aint0;
    if (aint0 != 0xff)
        failures++;

    // ------------------------------------------------------------------------
    return (failures == 0);
}

// ----------------------------------------------------------------------------
// ALU AND CPU TEST 5
// ----------------------------------------------------------------------------
bool cpu_test_5()
{
    unsigned char failures = 0;
    unsigned char achar0 = 0;
    unsigned char achar1 = 0;
    unsigned char achar2 = 0;
    achar0 <<= 1;
    if (achar0 != achar1)
        failures++;
    achar0 = 1;
    achar1 = 2;
    for (achar2 = 0; achar2 < 6; achar2++)
    {
        achar0 <<= 1;
        if (achar0 != achar1)
            failures++;
        achar1 <<= 1;
    }
    achar0 = 1;
    achar1 = 4;
    achar0 <<= 2;
    if (achar0 != achar1)
        failures++;
    achar0 = 1;
    achar1 = 8;
    achar0 <<= 3;
    if (achar0 != achar1)
        failures++;
    achar0 = 1;
    achar1 = 0x10;
    achar0 <<= 4;
    if (achar0 != achar1)
        failures++;
    achar0 = 1;
    achar1 = 0x20;
    achar0 <<= 5;
    if (achar0 != achar1)
        failures++;
    achar0 = 1;
    achar1 = 0x40;
    achar0 <<= 6;
    if (achar0 != achar1)
        failures++;
    achar0 = 1;
    achar1 = 0x80;
    achar0 <<= 7;
    if (achar0 != achar1)
        failures++;
    achar0 = 2;
    achar1 = 1;
    achar0 >>= 1;
    if (achar0 != achar1)
        failures++;
    achar0 = 4;
    achar0 >>= 2;
    if (achar0 != achar1)
        failures++;
    achar0 = 8;
    achar0 >>= 3;
    if (achar0 != achar1)
        failures++;
    achar0 = 0x10;
    achar0 >>= 4;
    if (achar0 != achar1)
        failures++;
    achar0 = 0x20;
    achar0 >>= 5;
    if (achar0 != achar1)
        failures++;
    achar0 = 0x40;
    achar0 >>= 6;
    if (achar0 != achar1)
        failures++;
    achar0 = 0x80;
    achar0 >>= 7;
    if (achar0 != achar1)
        failures++;
    return (failures == 0);
}

// ----------------------------------------------------------------------------
// ALU AND CPU TEST 6
// ----------------------------------------------------------------------------
bool cpu_test_6()
{
    unsigned char failures = 0;
    signed char c1, c2, c3;
    unsigned char uc1, uc2;
    unsigned int ui1, ui2, ui3;
    signed int i1;
    uint8_t vuc;
    // ------------------------------------------------------------------------
    c1 = 1;
    c3 = 5;
    c1 = c1 * 5;       /* char = char * lit */
    c2 = c1 * c3;     /* char = char * char */
    if (c2 != 25)
        failures++;

    // ------------------------------------------------------------------------
    uc1 = 0x10;
    uc2 = uc1 * 2;
    if (uc2 != 0x20)
        failures++;

    // ------------------------------------------------------------------------
    vuc = 0x05;
    ui1 = uc1 * uc2;
    i1 = c1 * c2;
    ui3 = ui1 * ui2;
    uc2 = 0;
    uc1 = vuc;
    uc1 = uc1 * 1;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 2;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 3;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 4;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 5;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 6;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 7;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 8;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 9;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 10;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 11;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 12;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 13;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 14;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 15;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 16;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 17;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 18;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 19;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 20;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 21;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 22;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 23;
    if (uc1 != (uc2 += 0x05)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 24;
    if (uc1 != (uc2 += 0x05)) failures++;

    uc1 = vuc;
    uc1 = uc1 * 31;
    if (uc1 != ((31 * 0x05) & 0xff)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 32;
    if (uc1 != ((32 * 0x05) & 0xff)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 64;
    if (uc1 != ((64 * 0x05) & 0xff)) failures++;
    uc1 = vuc;
    uc1 = uc1 * 128;
    if (uc1 != ((128 * 0x05) & 0xff)) failures++;

    /* testing literal multiply with different source and destination */
    uc1 = vuc * 1;
    if (uc1 != ((1 * 0x05) & 0xff)) failures++;
    uc1 = vuc * 2;
    if (uc1 != ((2 * 0x05) & 0xff)) failures++;
    uc1 = vuc * 4;
    if (uc1 != ((4 * 0x05) & 0xff)) failures++;

    // ------------------------------------------------------------------------
    return (failures == 0);
}
