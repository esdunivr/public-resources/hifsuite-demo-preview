#ifndef DEFINES
#define DEFINES

/// The maximum length of a string.
#define MAX_STRING_LENGTH 125

/// Function used to access memory.
#define MEMORY_ACCESS(address) (*(volatile char*)(address))

/// Starting address of the stack.
#define STACK_START_ADDRESS 0x1C00

#endif