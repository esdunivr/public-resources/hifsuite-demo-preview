//----------------------------------------------------------
//-- TITLE: Controller of a watertank system
//--
//-- Author: Stefano Centomo
//--
//-- Date:15/06/2018
//-- Controller of a watertank system, runned on a m6502 cpu
//----------------------------------------------------------


#include <stdint.h>
#include "routines.h"

//----------------------------
//	FIXED POINT DIMENSIONS 
//----------------------------
#define FIXED_WIDTH	16													//	data width
#define FIXED_WHOLE_WIDTH	8											//	whole data width
#define FIXED_FRACTIONAL_WIDTH	(FIXED_WIDTH - FIXED_WHOLE_WIDTH)		//	fractional data witdh

//--------------------------------------
//	CONTROLLER STATES DEFINITION
//--------------------------------------

typedef enum {ACTIVE,INCREASE,NOTHING,DECREASE} STATES;

//--------------------------------------------
//	CONSTANTS (8 bits  precision)  
//   8 fractional, 8 whole
//--------------------------------------------
//                                                  bit positions
//                                                    1234 5678
//                                                      b   3
#define START_THRESHOLD 0x00b3	// 0.7 ~ 0.69921875 = 1011 0011 = 1/(2^1) + 1/(2^3) + 1/(2^4) + 1/(2^7) + 1/(2^8)
//                                                      1   9
#define OPEN_CONSTANT	0x0019	// 0.1 ~ 0.09765625 = 0001 1001 = 1/(2^4) + 1/(2^5) + 1/(2^8)
//                                                      4   c
#define CLOSE_CONSTANT	0x004c	// 0.3 ~ 0.29687500 = 0100 1100 = 1/(2^2) + 1/(2^4) + 1/(2^5) 

#define MAX_APERTURE	0x0300	// 3.0


void firmware(void);

