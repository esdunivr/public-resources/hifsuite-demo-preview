//-------------------------------------------------------------
// Title:	Routines Library for m6502 connected to a platform
// Author:	Stefano Centomo
// Date:	15/06/2018
//-------------------------------------------------------------
#include "mmio.h"
#include "stdbool.h"
// Send op1 and op2 to multiplier connected to psel2, and return the result.
uint32_t mul(uint16_t op1, uint16_t op2);
// Read data from the accelerometer peripheral and save in cap variables
void read_accelerometer(uint8_t * cap7, uint8_t * cap5, uint8_t * cap2, uint8_t * cap1);
// Send data through the network interface
void net_send_data(uint32_t data);
// Receive data from the network interface
uint32_t net_receive_data(void);

uint8_t get_random_number(void);
// Read data from the Sensor peripheral
void watertank_sense(bool *high, bool *low);
//Send data through the Actuator peripheral  
void watertank_actuate(bool open, bool close, uint16_t threshold);
// Start the watchdog with a limit
void start_counter(uint16_t max_counter);
//End counting 
uint8_t  end_counter(uint16_t * actual_counter);