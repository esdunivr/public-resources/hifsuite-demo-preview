// /////////////////////////////////////////////////////////////////////////
// SystemC code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#ifndef AMBA_APB_BUS_HH
#define AMBA_APB_BUS_HH

#include <stdint.h>
#include <systemc>

class amba_apb_bus :
    public sc_core::sc_module
{

public:

    sc_core::sc_in< bool > clk;
    sc_core::sc_out< bool > apb_master_pclk;
    sc_core::sc_in< bool > apb_master_presetn;
    sc_core::sc_in< uint32_t > apb_master_paddr;
    sc_core::sc_in< bool > apb_master_psel1;
    sc_core::sc_in< bool > apb_master_psel2;
    sc_core::sc_in< bool > apb_master_psel3;
    sc_core::sc_in< bool > apb_master_psel4;
    sc_core::sc_in< bool > apb_master_psel5;
    sc_core::sc_in< bool > apb_master_psel6;
    sc_core::sc_in< bool > apb_master_psel7;
    sc_core::sc_in< bool > apb_master_psel8;
    sc_core::sc_in< bool > apb_master_penable;
    sc_core::sc_in< bool > apb_master_pwrite;
    sc_core::sc_in< uint32_t > apb_master_pwdata;
    sc_core::sc_out< bool > apb_master_pready;
    sc_core::sc_out< uint32_t > apb_master_prdata;
    sc_core::sc_out< bool > apb_1_pclk;
    sc_core::sc_out< bool > apb_1_presetn;
    sc_core::sc_out< uint32_t > apb_1_paddr;
    sc_core::sc_out< bool > apb_1_psel;
    sc_core::sc_out< bool > apb_1_penable;
    sc_core::sc_out< bool > apb_1_pwrite;
    sc_core::sc_out< uint32_t > apb_1_pwdata;
    sc_core::sc_in< bool > apb_1_pready;
    sc_core::sc_in< uint32_t > apb_1_prdata;
    sc_core::sc_out< bool > apb_2_pclk;
    sc_core::sc_out< bool > apb_2_presetn;
    sc_core::sc_out< uint32_t > apb_2_paddr;
    sc_core::sc_out< bool > apb_2_psel;
    sc_core::sc_out< bool > apb_2_penable;
    sc_core::sc_out< bool > apb_2_pwrite;
    sc_core::sc_out< uint32_t > apb_2_pwdata;
    sc_core::sc_in< bool > apb_2_pready;
    sc_core::sc_in< uint32_t > apb_2_prdata;
    sc_core::sc_out< bool > apb_3_pclk;
    sc_core::sc_out< bool > apb_3_presetn;
    sc_core::sc_out< uint32_t > apb_3_paddr;
    sc_core::sc_out< bool > apb_3_psel;
    sc_core::sc_out< bool > apb_3_penable;
    sc_core::sc_out< bool > apb_3_pwrite;
    sc_core::sc_out< uint32_t > apb_3_pwdata;
    sc_core::sc_in< bool > apb_3_pready;
    sc_core::sc_in< uint32_t > apb_3_prdata;
    sc_core::sc_out< bool > apb_4_pclk;
    sc_core::sc_out< bool > apb_4_presetn;
    sc_core::sc_out< uint32_t > apb_4_paddr;
    sc_core::sc_out< bool > apb_4_psel;
    sc_core::sc_out< bool > apb_4_penable;
    sc_core::sc_out< bool > apb_4_pwrite;
    sc_core::sc_out< uint32_t > apb_4_pwdata;
    sc_core::sc_in< bool > apb_4_pready;
    sc_core::sc_in< uint32_t > apb_4_prdata;
    sc_core::sc_out< bool > apb_5_pclk;
    sc_core::sc_out< bool > apb_5_presetn;
    sc_core::sc_out< uint32_t > apb_5_paddr;
    sc_core::sc_out< bool > apb_5_psel;
    sc_core::sc_out< bool > apb_5_penable;
    sc_core::sc_out< bool > apb_5_pwrite;
    sc_core::sc_out< uint32_t > apb_5_pwdata;
    sc_core::sc_in< bool > apb_5_pready;
    sc_core::sc_in< uint32_t > apb_5_prdata;
    sc_core::sc_out< bool > apb_6_pclk;
    sc_core::sc_out< bool > apb_6_presetn;
    sc_core::sc_out< uint32_t > apb_6_paddr;
    sc_core::sc_out< bool > apb_6_psel;
    sc_core::sc_out< bool > apb_6_penable;
    sc_core::sc_out< bool > apb_6_pwrite;
    sc_core::sc_out< uint32_t > apb_6_pwdata;
    sc_core::sc_in< bool > apb_6_pready;
    sc_core::sc_in< uint32_t > apb_6_prdata;
    sc_core::sc_out< bool > apb_7_pclk;
    sc_core::sc_out< bool > apb_7_presetn;
    sc_core::sc_out< uint32_t > apb_7_paddr;
    sc_core::sc_out< bool > apb_7_psel;
    sc_core::sc_out< bool > apb_7_penable;
    sc_core::sc_out< bool > apb_7_pwrite;
    sc_core::sc_out< uint32_t > apb_7_pwdata;
    sc_core::sc_in< bool > apb_7_pready;
    sc_core::sc_in< uint32_t > apb_7_prdata;
    sc_core::sc_out< bool > apb_8_pclk;
    sc_core::sc_out< bool > apb_8_presetn;
    sc_core::sc_out< uint32_t > apb_8_paddr;
    sc_core::sc_out< bool > apb_8_psel;
    sc_core::sc_out< bool > apb_8_penable;
    sc_core::sc_out< bool > apb_8_pwrite;
    sc_core::sc_out< uint32_t > apb_8_pwdata;
    sc_core::sc_in< bool > apb_8_pready;
    sc_core::sc_in< uint32_t > apb_8_prdata;
    sc_core::sc_signal< bool > clkSuphif_0;


    SC_HAS_PROCESS( amba_apb_bus );

    amba_apb_bus( sc_core::sc_module_name name_ );
    ~amba_apb_bus();


private:

    amba_apb_bus( const amba_apb_bus & );
    const amba_apb_bus& operator= ( const amba_apb_bus & );


    void vhdl_process();
    void UpdclkSuphif_0();

};


#endif

