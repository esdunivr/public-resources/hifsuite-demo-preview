// /////////////////////////////////////////////////////////////////////////
// SystemC code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#ifndef MULTIPLIER_APB_WRAPPER_HH
#define MULTIPLIER_APB_WRAPPER_HH

#include <stdint.h>
#include <systemc>
#include "multiplier.hpp"

class multiplier_apb_wrapper :
    public sc_core::sc_module
{

public:

    sc_core::sc_in< bool > pclk;
    sc_core::sc_in< bool > presetn;
    sc_core::sc_in< uint32_t > paddr;
    sc_core::sc_in< bool > psel;
    sc_core::sc_in< bool > penable;
    sc_core::sc_in< bool > pwrite;
    sc_core::sc_in< uint32_t > pwdata;
    sc_core::sc_out< bool > pready;
    sc_core::sc_out< uint32_t > prdata;
    sc_core::sc_signal< bool > clk;
    sc_core::sc_signal< bool > din_rdy;
    sc_core::sc_signal< uint16_t > op1;
    sc_core::sc_signal< uint16_t > op2;
    sc_core::sc_signal< uint32_t > result;
    sc_core::sc_signal< bool > dout_rdy;

    multiplier multiplier_0;


    SC_HAS_PROCESS( multiplier_apb_wrapper );

    multiplier_apb_wrapper( sc_core::sc_module_name name_ );
    ~multiplier_apb_wrapper();


private:

    multiplier_apb_wrapper( const multiplier_apb_wrapper & );
    const multiplier_apb_wrapper& operator= ( const multiplier_apb_wrapper & );


    void globact_process_5();
    void globact_process_6();
    void globact_process_7();
    void globact_process_8();
    void globact_process_9();
    void globact_process_10();

};


#endif

