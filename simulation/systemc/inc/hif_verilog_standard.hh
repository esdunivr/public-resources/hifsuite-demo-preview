/// @file   hif_verilog_standard.hh
/// @author Enrico Fraccaroli
/// @date   Sep 05 2017

#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>

namespace hif_verilog_standard
{

inline bool hif_verilog__system_readmemh(
    const std::string & fileName,
    uint8_t * memory,
    const size_t & start_x,
    const size_t & end_x,
    const size_t & start_y,
    const size_t & end_y)
{
    std::ifstream infile(fileName.c_str(), std::ifstream::in);
    if (!infile.is_open())
    {
        return false;
    }
    int i =0;
    int a =0;
    while(!infile.eof()) {

      infile >> std::hex >> a;
      memory[i] = a;
      i++;

    }


    infile.close();
    return true;
}
}
