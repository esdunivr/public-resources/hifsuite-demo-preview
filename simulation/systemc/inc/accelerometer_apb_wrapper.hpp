// /////////////////////////////////////////////////////////////////////////
// SystemC code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#ifndef ACCELEROMETER_APB_WRAPPER_HH
#define ACCELEROMETER_APB_WRAPPER_HH

#include <stdint.h>
#include <systemc>
#include "hif_globals.hpp"

class accelerometer_apb_wrapper :
    public sc_core::sc_module
{

public:

    sc_core::sc_in< bool > pclk;
    sc_core::sc_in< bool > presetn;
    sc_core::sc_in< uint32_t > paddr;
    sc_core::sc_in< bool > psel;
    sc_core::sc_in< bool > penable;
    sc_core::sc_in< bool > pwrite;
    sc_core::sc_in< uint32_t > pwdata;
    sc_core::sc_out< bool > pready;
    sc_core::sc_out< uint32_t > prdata;

    // Input analog clock.
    sc_core::sc_in< bool > analog_clock;
    uint32_t prdata_var;
    double ddt_7;
    double ddt_6;
    double ddt_5;
    double ddt_4;
    double ddt_3;
    double ddt_2;
    double ddt_1;
    double ddt_0;
    double ddt;
    sc_core::sc_signal< uint8_t > cap7_d;
    uint8_t cap7_d_var;
    sc_core::sc_signal< uint8_t > cap5_d;
    uint8_t cap5_d_var;
    sc_core::sc_signal< uint8_t > cap2_d;
    uint8_t cap2_d_var;
    sc_core::sc_signal< uint8_t > cap1_d;
    uint8_t cap1_d_var;
    double acc_cap7_sample;
    double acc_cap5_sample;
    double acc_cap2_sample;
    double acc_cap1_sample;
    bool acc_cap7_over;
    bool acc_cap5_over;
    bool acc_cap2_over;
    bool acc_cap1_over;
    int32_t acc_i;

    // List of branch variables.
    AnalogPair b_avx;
    AnalogPair b_avy;
    AnalogPair b_avz;
    AnalogPair b_acc_reduced_0;
    AnalogPair b_acc_reduced_1;
    AnalogPair b_acc_reduced_0_dot;
    AnalogPair b_acc_reduced_1_dot;
    AnalogPair b_acc_avx_dot;
    AnalogPair b_acc_avy_dot;
    AnalogPair b_acc_avz_dot;
    AnalogPair b_acc_Cap1;
    AnalogPair b_acc_Cap2;
    AnalogPair b_acc_Cap5;
    AnalogPair b_acc_Cap7;


    SC_HAS_PROCESS( accelerometer_apb_wrapper );

    accelerometer_apb_wrapper( sc_core::sc_module_name name_ );
    ~accelerometer_apb_wrapper();


private:

    accelerometer_apb_wrapper( const accelerometer_apb_wrapper & );
    const accelerometer_apb_wrapper& operator= ( const accelerometer_apb_wrapper
         & );


    void process_1();
    void process_2();
    void acc_process();
    void acc_process_0();

};


#endif

