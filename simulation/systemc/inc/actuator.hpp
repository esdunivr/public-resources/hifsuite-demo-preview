// /////////////////////////////////////////////////////////////////////////
// SystemC code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#ifndef ACTUATOR_HH
#define ACTUATOR_HH

#include <stdint.h>
#include <systemc>

class actuator :
    public sc_core::sc_module
{

public:

    sc_core::sc_in< bool > pclk;
    sc_core::sc_in< bool > presetn;
    sc_core::sc_in< uint32_t > paddr;
    sc_core::sc_in< bool > psel;
    sc_core::sc_in< bool > penable;
    sc_core::sc_in< bool > pwrite;
    sc_core::sc_in< uint32_t > pwdata;
    sc_core::sc_out< bool > pready;
    sc_core::sc_out< uint32_t > prdata;
    sc_core::sc_out< bool > actuator_open;
    sc_core::sc_out< bool > actuator_close;
    sc_core::sc_out< uint16_t > actuator_threshold;
    sc_core::sc_signal< bool > actuator_open_reg;
    sc_core::sc_signal< bool > actuator_close_reg;
    sc_core::sc_signal< uint16_t > actuator_threshold_reg;

    void start_of_simulation();



    SC_HAS_PROCESS( actuator );

    actuator( sc_core::sc_module_name name_ );
    ~actuator();


private:

    actuator( const actuator & );
    const actuator& operator= ( const actuator & );


    void process();

};


#endif

