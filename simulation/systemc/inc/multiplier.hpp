// /////////////////////////////////////////////////////////////////////////
// SystemC code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#ifndef MULTIPLIER_HH
#define MULTIPLIER_HH

#include <stdint.h>
#include <systemc>

class multiplier :
    public sc_core::sc_module
{

public:

    sc_core::sc_in< bool > clk;
    sc_core::sc_in< bool > din_rdy;
    sc_core::sc_in< uint16_t > op1;
    sc_core::sc_in< uint16_t > op2;
    sc_core::sc_out< uint32_t > result;
    sc_core::sc_out< bool > dout_rdy;
    sc_core::sc_signal< bool > clkSuphif_1;


    SC_HAS_PROCESS( multiplier );

    multiplier( sc_core::sc_module_name name_ );
    ~multiplier();


private:

    multiplier( const multiplier & );
    const multiplier& operator= ( const multiplier & );


    void vhdl_process();
    void UpdclkSuphif_1();

};


#endif

