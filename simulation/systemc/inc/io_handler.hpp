#include "complete_platform.hpp"
#include "hif_globals.hpp"

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream

// ----------------------------------------------------------------------------
// COLORS
// ----------------------------------------------------------------------------
#define KRST  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define FRED(x) KRED x KRST
#define FGRN(x) KGRN x KRST
#define FYEL(x) KYEL x KRST
#define FBLU(x) KBLU x KRST
#define FMAG(x) KMAG x KRST
#define FCYN(x) KCYN x KRST
#define FWHT(x) KWHT x KRST

#define BOLD(x) "\x1B[1m" x KRST
#define UNDL(x) "\x1B[4m" x KRST

// ----------------------------------------------------------------------------
// MEMORY ADDRESSES
// ----------------------------------------------------------------------------
// Starting address for the stack.
#define STACK_START_ADDRESS 0x1C00

// Memory locations used to determine the application status.
#define STATUS    (STACK_START_ADDRESS + 0x001A)

// Specific address used for putchar function.
#define PUTCHAR_ADDRESS (STACK_START_ADDRESS + 0x0029)
#define GETCHAR_ADDRESS (STACK_START_ADDRESS + 0x002A)
#define GETCHAR_ASSERT  (STACK_START_ADDRESS + 0x002B)

/// The maximum length of a string.
#define MAX_STRING_LENGTH 125

class io_handler :
    public sc_core::sc_module
{
private:
    // Pointer to the platform
    complete_platform * _platform;
    // String buffer.
    std::string _getchar_buffer;
    // Variable used to enable getchar.
    bool _getchar_assert_previous;

public:
    sc_core::sc_in<bool> clk;

    SC_HAS_PROCESS(io_handler);

    io_handler(sc_core::sc_module_name name_,
               complete_platform * platform) :
        sc_core::sc_module(name_),
        clk("clk"),
        _platform(platform),
        _getchar_buffer(),
        _getchar_assert_previous(true)
    {
        SC_METHOD(io_process);
        sensitive << clk.pos();
        SC_METHOD(status_exit);
        sensitive << clk;
    }

    // Loads the rom file into the platform ROM.
    bool load_rom(std::string const & rom)
    {
        printf("Loading rom '%s'...\n", rom.c_str());
        std::ifstream infile(rom.c_str(), std::ifstream::in);
        int a;
        int i = 0;
        while (!infile.eof())
        {
            infile >> std::hex >> a;
            _platform->memory_0._rom[i] = a;
            i++;
        }
        infile.close();
        printf("Done\n");
        return true;
    }

    // Code to handle getchar and putchar.
    void io_process()
    {
        if (_platform->memory_0.ram_addr == PUTCHAR_ADDRESS)
        {
            std::cout << KGRN << char(_platform->memory_0._ram[PUTCHAR_ADDRESS]) << KRST;
        }
        else if (_platform->memory_0.ram_addr == GETCHAR_ASSERT)
        {
            if (_platform->memory_0._ram[GETCHAR_ASSERT] != _getchar_assert_previous)
            {
                if(_getchar_buffer.empty())
                {
                    // Get the input.
                    std::getline(std::cin, _getchar_buffer);
                    // Push \n because getline removes it.
                    _getchar_buffer.push_back('\n');
                }
                // Push the front.
                _platform->memory_0._ram[GETCHAR_ADDRESS] = _getchar_buffer.front();
                // Remove the front.
                _getchar_buffer.erase(0, 1);
                // Toggle the asser.
                _getchar_assert_previous = _platform->memory_0._ram[GETCHAR_ASSERT];
            }
        }
    }

    // Code to handle code termination.
    void status_exit()
    {
        if (_platform->memory_0._ram[STATUS] == 1)
        {
            sc_core::sc_stop();
        }
        // Update analog time.
        analogTime += 50E-09;
    }
};
