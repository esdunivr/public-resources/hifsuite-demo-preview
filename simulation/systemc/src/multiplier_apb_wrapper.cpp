// /////////////////////////////////////////////////////////////////////////
// SystemC code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#include "../inc/multiplier_apb_wrapper.hpp"



multiplier_apb_wrapper::multiplier_apb_wrapper( sc_core::sc_module_name name_ )
     :
    sc_core::sc_module( name_ ),
    pclk("pclk"),
    presetn("presetn"),
    paddr("paddr"),
    psel("psel"),
    penable("penable"),
    pwrite("pwrite"),
    pwdata("pwdata"),
    pready("pready"),
    prdata("prdata"),
    clk("clk"),
    din_rdy("din_rdy"),
    op1("op1"),
    op2("op2"),
    result("result"),
    dout_rdy("dout_rdy"),
    multiplier_0("multiplier_0")
{
    pready.initialize( false );
    prdata.initialize( 0UL );
    clk = false;
    din_rdy = false;
    op1 = uint16_t(0U);
    op2 = uint16_t(0U);
    result = 0UL;
    dout_rdy = false;

    multiplier_0.clk( clk );
    multiplier_0.din_rdy( din_rdy );
    multiplier_0.op1( op1 );
    multiplier_0.op2( op2 );
    multiplier_0.result( result );
    multiplier_0.dout_rdy( dout_rdy );

    SC_METHOD( globact_process_5 );
    sensitive << pclk;

    SC_METHOD( globact_process_6 );
    sensitive << pwdata;

    SC_METHOD( globact_process_7 );
    sensitive << pwdata;

    SC_METHOD( globact_process_8 );
    sensitive << penable;

    SC_METHOD( globact_process_9 );
    sensitive << dout_rdy;

    SC_METHOD( globact_process_10 );
    sensitive << result;
}

multiplier_apb_wrapper::~multiplier_apb_wrapper()
{}

void multiplier_apb_wrapper::globact_process_5()
{
    clk = pclk.read();
}

void multiplier_apb_wrapper::globact_process_6()
{
    op1 = pwdata.read();
}

void multiplier_apb_wrapper::globact_process_7()
{
    op2 = pwdata.read() >> 16L & 65535UL;
}

void multiplier_apb_wrapper::globact_process_8()
{
    din_rdy = penable.read();
}

void multiplier_apb_wrapper::globact_process_9()
{
    pready = dout_rdy.read();
}

void multiplier_apb_wrapper::globact_process_10()
{
    prdata = result.read();
}



