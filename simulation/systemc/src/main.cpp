#include "complete_platform.hpp"
#include "io_handler.hpp"

int sc_main(int argc, char * argv[])
{
    sc_core::sc_clock clk("clk", sc_core::sc_time(50, sc_core::SC_NS));
    
    sc_core::sc_signal<bool> rst("rst");
    sc_core::sc_signal<bool> sensor_high("sensor_high");
    sc_core::sc_signal<bool> sensor_low("sensor_low");
    sc_core::sc_signal<bool> actuator_open("actuator_open");
    sc_core::sc_signal<bool> actuator_close("actuator_close");
    sc_core::sc_signal<uint16_t> actuator_threshold("actuator_threshold");

    complete_platform completePlatform("CP");
    completePlatform.clk(clk);
    completePlatform.rst(rst);
    completePlatform.sensor_high(sensor_high);
    completePlatform.sensor_low(sensor_low);
    completePlatform.actuator_open(actuator_open);
    completePlatform.actuator_close(actuator_close);
    completePlatform.actuator_threshold(actuator_threshold);

    // Create an io handler.
    io_handler io_handler_0("io_handler_0", &completePlatform);
    io_handler_0.clk(clk);
    // Load the ROM.
    io_handler_0.load_rom("../roms/rom.mem");

    rst.write(false);

    // Simulate.
    printf("Simulating...\n");
    sc_core::sc_start(1, sc_core::SC_SEC);
    printf("Done\n");
    return 0;
}
