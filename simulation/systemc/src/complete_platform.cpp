// /////////////////////////////////////////////////////////////////////////
// SystemC code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#include "../inc/complete_platform.hpp"



complete_platform::complete_platform( sc_core::sc_module_name name_ ) :
    sc_core::sc_module( name_ ),
    clk("clk"),
    rst("rst"),
    sensor_high("sensor_high"),
    sensor_low("sensor_low"),
    actuator_open("actuator_open"),
    actuator_close("actuator_close"),
    actuator_threshold("actuator_threshold"),
    apb_master_pclk("apb_master_pclk"),
    apb_master_presetn("apb_master_presetn"),
    apb_master_paddr("apb_master_paddr"),
    apb_master_psel1("apb_master_psel1"),
    apb_master_psel2("apb_master_psel2"),
    apb_master_psel3("apb_master_psel3"),
    apb_master_psel4("apb_master_psel4"),
    apb_master_psel5("apb_master_psel5"),
    apb_master_psel6("apb_master_psel6"),
    apb_master_psel7("apb_master_psel7"),
    apb_master_psel8("apb_master_psel8"),
    apb_master_penable("apb_master_penable"),
    apb_master_pwrite("apb_master_pwrite"),
    apb_master_pwdata("apb_master_pwdata"),
    apb_master_pready("apb_master_pready"),
    apb_master_prdata("apb_master_prdata"),
    apb_1_pclk("apb_1_pclk"),
    apb_1_presetn("apb_1_presetn"),
    apb_1_paddr("apb_1_paddr"),
    apb_1_psel("apb_1_psel"),
    apb_1_penable("apb_1_penable"),
    apb_1_pwrite("apb_1_pwrite"),
    apb_1_pwdata("apb_1_pwdata"),
    apb_1_pready("apb_1_pready"),
    apb_1_prdata("apb_1_prdata"),
    apb_2_pclk("apb_2_pclk"),
    apb_2_presetn("apb_2_presetn"),
    apb_2_paddr("apb_2_paddr"),
    apb_2_psel("apb_2_psel"),
    apb_2_penable("apb_2_penable"),
    apb_2_pwrite("apb_2_pwrite"),
    apb_2_pwdata("apb_2_pwdata"),
    apb_2_pready("apb_2_pready"),
    apb_2_prdata("apb_2_prdata"),
    apb_3_pclk("apb_3_pclk"),
    apb_3_presetn("apb_3_presetn"),
    apb_3_paddr("apb_3_paddr"),
    apb_3_psel("apb_3_psel"),
    apb_3_penable("apb_3_penable"),
    apb_3_pwrite("apb_3_pwrite"),
    apb_3_pwdata("apb_3_pwdata"),
    apb_3_pready("apb_3_pready"),
    apb_3_prdata("apb_3_prdata"),
    apb_4_pclk("apb_4_pclk"),
    apb_4_presetn("apb_4_presetn"),
    apb_4_paddr("apb_4_paddr"),
    apb_4_psel("apb_4_psel"),
    apb_4_penable("apb_4_penable"),
    apb_4_pwrite("apb_4_pwrite"),
    apb_4_pwdata("apb_4_pwdata"),
    apb_4_pready("apb_4_pready"),
    apb_4_prdata("apb_4_prdata"),
    apb_5_pclk("apb_5_pclk"),
    apb_5_presetn("apb_5_presetn"),
    apb_5_paddr("apb_5_paddr"),
    apb_5_psel("apb_5_psel"),
    apb_5_penable("apb_5_penable"),
    apb_5_pwrite("apb_5_pwrite"),
    apb_5_pwdata("apb_5_pwdata"),
    apb_5_pready("apb_5_pready"),
    apb_5_prdata("apb_5_prdata"),
    apb_6_pclk("apb_6_pclk"),
    apb_6_presetn("apb_6_presetn"),
    apb_6_paddr("apb_6_paddr"),
    apb_6_psel("apb_6_psel"),
    apb_6_penable("apb_6_penable"),
    apb_6_pwrite("apb_6_pwrite"),
    apb_6_pwdata("apb_6_pwdata"),
    apb_6_pready("apb_6_pready"),
    apb_6_prdata("apb_6_prdata"),
    apb_7_pclk("apb_7_pclk"),
    apb_7_presetn("apb_7_presetn"),
    apb_7_paddr("apb_7_paddr"),
    apb_7_psel("apb_7_psel"),
    apb_7_penable("apb_7_penable"),
    apb_7_pwrite("apb_7_pwrite"),
    apb_7_pwdata("apb_7_pwdata"),
    apb_7_pready("apb_7_pready"),
    apb_7_prdata("apb_7_prdata"),
    apb_8_pclk("apb_8_pclk"),
    apb_8_presetn("apb_8_presetn"),
    apb_8_paddr("apb_8_paddr"),
    apb_8_psel("apb_8_psel"),
    apb_8_penable("apb_8_penable"),
    apb_8_pwrite("apb_8_pwrite"),
    apb_8_pwdata("apb_8_pwdata"),
    apb_8_pready("apb_8_pready"),
    apb_8_prdata("apb_8_prdata"),
    addr("addr"),
    datai("datai"),
    datao("datao"),
    oeb("oeb"),
    irq_n("irq_n"),
    nmi_n("nmi_n"),
    res_n("res_n"),
    rdy("rdy"),
    sob_n("sob_n"),
    sync("sync"),
    vpab("vpab"),
    we_n("we_n"),
    m6502_0("m6502_0"),
    memory_0("memory_0"),
    amba_apb_bus_0("amba_apb_bus_0"),
    multiplier_0("multiplier_0"),
    accelerometer_0("accelerometer_0"),
    network_interface_0("network_interface_0"),
    lfsr_0("lfsr_0"),
    sensor_0("sensor_0"),
    actuator_0("actuator_0"),
    watchdog_0("watchdog_0")
{
    actuator_open.initialize( false );
    actuator_close.initialize( false );
    actuator_threshold.initialize( uint16_t(0U) );
    apb_master_pclk = false;
    apb_master_presetn = false;
    apb_master_paddr = 0UL;
    apb_master_psel1 = false;
    apb_master_psel2 = false;
    apb_master_psel3 = false;
    apb_master_psel4 = false;
    apb_master_psel5 = false;
    apb_master_psel6 = false;
    apb_master_psel7 = false;
    apb_master_psel8 = false;
    apb_master_penable = false;
    apb_master_pwrite = false;
    apb_master_pwdata = 0UL;
    apb_master_pready = false;
    apb_master_prdata = 0UL;
    apb_1_pclk = false;
    apb_1_presetn = false;
    apb_1_paddr = 0UL;
    apb_1_psel = false;
    apb_1_penable = false;
    apb_1_pwrite = false;
    apb_1_pwdata = 0UL;
    apb_1_pready = false;
    apb_1_prdata = 0UL;
    apb_2_pclk = false;
    apb_2_presetn = false;
    apb_2_paddr = 0UL;
    apb_2_psel = false;
    apb_2_penable = false;
    apb_2_pwrite = false;
    apb_2_pwdata = 0UL;
    apb_2_pready = false;
    apb_2_prdata = 0UL;
    apb_3_pclk = false;
    apb_3_presetn = false;
    apb_3_paddr = 0UL;
    apb_3_psel = false;
    apb_3_penable = false;
    apb_3_pwrite = false;
    apb_3_pwdata = 0UL;
    apb_3_pready = false;
    apb_3_prdata = 0UL;
    apb_4_pclk = false;
    apb_4_presetn = false;
    apb_4_paddr = 0UL;
    apb_4_psel = false;
    apb_4_penable = false;
    apb_4_pwrite = false;
    apb_4_pwdata = 0UL;
    apb_4_pready = false;
    apb_4_prdata = 0UL;
    apb_5_pclk = false;
    apb_5_presetn = false;
    apb_5_paddr = 0UL;
    apb_5_psel = false;
    apb_5_penable = false;
    apb_5_pwrite = false;
    apb_5_pwdata = 0UL;
    apb_5_pready = false;
    apb_5_prdata = 0UL;
    apb_6_pclk = false;
    apb_6_presetn = false;
    apb_6_paddr = 0UL;
    apb_6_psel = false;
    apb_6_penable = false;
    apb_6_pwrite = false;
    apb_6_pwdata = 0UL;
    apb_6_pready = false;
    apb_6_prdata = 0UL;
    apb_7_pclk = false;
    apb_7_presetn = false;
    apb_7_paddr = 0UL;
    apb_7_psel = false;
    apb_7_penable = false;
    apb_7_pwrite = false;
    apb_7_pwdata = 0UL;
    apb_7_pready = false;
    apb_7_prdata = 0UL;
    apb_8_pclk = false;
    apb_8_presetn = false;
    apb_8_paddr = 0UL;
    apb_8_psel = false;
    apb_8_penable = false;
    apb_8_pwrite = false;
    apb_8_pwdata = 0UL;
    apb_8_pready = false;
    apb_8_prdata = 0UL;
    addr = uint16_t(0U);
    datai = uint8_t(0U);
    datao = uint8_t(0U);
    oeb = false;
    irq_n = false;
    nmi_n = false;
    res_n = false;
    rdy = false;
    sob_n = false;
    sync = false;
    vpab = false;
    we_n = false;

    m6502_0.clk( clk );
    m6502_0.addr( addr );
    m6502_0.datai( datai );
    m6502_0.datao( datao );
    m6502_0.irq_n( irq_n );
    m6502_0.nmi_n( nmi_n );
    m6502_0.sob_n( sob_n );
    m6502_0.res_n( res_n );
    m6502_0.rdy( rdy );
    m6502_0.vpab( vpab );
    m6502_0.sync( sync );
    m6502_0.we_n( we_n );
    m6502_0.oeb( oeb );

    memory_0.clk( clk );
    memory_0.rst( rst );
    memory_0.addr( addr );
    memory_0.datai( datai );
    memory_0.datao( datao );
    memory_0.irq_n( irq_n );
    memory_0.nmi_n( nmi_n );
    memory_0.sob_n( sob_n );
    memory_0.res_n( res_n );
    memory_0.rdy( rdy );
    memory_0.vpab( vpab );
    memory_0.sync( sync );
    memory_0.we_n( we_n );
    memory_0.oeb( oeb );
    memory_0.psel1( apb_master_psel1 );
    memory_0.psel2( apb_master_psel2 );
    memory_0.psel3( apb_master_psel3 );
    memory_0.psel4( apb_master_psel4 );
    memory_0.psel5( apb_master_psel5 );
    memory_0.psel6( apb_master_psel6 );
    memory_0.psel7( apb_master_psel7 );
    memory_0.psel8( apb_master_psel8 );
    memory_0.penable( apb_master_penable );
    memory_0.presetn( apb_master_presetn );
    memory_0.paddr( apb_master_paddr );
    memory_0.pwrite( apb_master_pwrite );
    memory_0.pwdata( apb_master_pwdata );
    memory_0.pready( apb_master_pready );
    memory_0.prdata( apb_master_prdata );

    amba_apb_bus_0.clk( clk );
    amba_apb_bus_0.apb_master_pclk( apb_master_pclk );
    amba_apb_bus_0.apb_master_presetn( apb_master_presetn );
    amba_apb_bus_0.apb_master_paddr( apb_master_paddr );
    amba_apb_bus_0.apb_master_psel1( apb_master_psel1 );
    amba_apb_bus_0.apb_master_psel2( apb_master_psel2 );
    amba_apb_bus_0.apb_master_psel3( apb_master_psel3 );
    amba_apb_bus_0.apb_master_psel4( apb_master_psel4 );
    amba_apb_bus_0.apb_master_psel5( apb_master_psel5 );
    amba_apb_bus_0.apb_master_psel6( apb_master_psel6 );
    amba_apb_bus_0.apb_master_psel7( apb_master_psel7 );
    amba_apb_bus_0.apb_master_psel8( apb_master_psel8 );
    amba_apb_bus_0.apb_master_penable( apb_master_penable );
    amba_apb_bus_0.apb_master_pwrite( apb_master_pwrite );
    amba_apb_bus_0.apb_master_pwdata( apb_master_pwdata );
    amba_apb_bus_0.apb_master_pready( apb_master_pready );
    amba_apb_bus_0.apb_master_prdata( apb_master_prdata );
    amba_apb_bus_0.apb_1_pclk( apb_1_pclk );
    amba_apb_bus_0.apb_1_presetn( apb_1_presetn );
    amba_apb_bus_0.apb_1_paddr( apb_1_paddr );
    amba_apb_bus_0.apb_1_psel( apb_1_psel );
    amba_apb_bus_0.apb_1_penable( apb_1_penable );
    amba_apb_bus_0.apb_1_pwrite( apb_1_pwrite );
    amba_apb_bus_0.apb_1_pwdata( apb_1_pwdata );
    amba_apb_bus_0.apb_1_pready( apb_1_pready );
    amba_apb_bus_0.apb_1_prdata( apb_1_prdata );
    amba_apb_bus_0.apb_2_pclk( apb_2_pclk );
    amba_apb_bus_0.apb_2_presetn( apb_2_presetn );
    amba_apb_bus_0.apb_2_paddr( apb_2_paddr );
    amba_apb_bus_0.apb_2_psel( apb_2_psel );
    amba_apb_bus_0.apb_2_penable( apb_2_penable );
    amba_apb_bus_0.apb_2_pwrite( apb_2_pwrite );
    amba_apb_bus_0.apb_2_pwdata( apb_2_pwdata );
    amba_apb_bus_0.apb_2_pready( apb_2_pready );
    amba_apb_bus_0.apb_2_prdata( apb_2_prdata );
    amba_apb_bus_0.apb_3_pclk( apb_3_pclk );
    amba_apb_bus_0.apb_3_presetn( apb_3_presetn );
    amba_apb_bus_0.apb_3_paddr( apb_3_paddr );
    amba_apb_bus_0.apb_3_psel( apb_3_psel );
    amba_apb_bus_0.apb_3_penable( apb_3_penable );
    amba_apb_bus_0.apb_3_pwrite( apb_3_pwrite );
    amba_apb_bus_0.apb_3_pwdata( apb_3_pwdata );
    amba_apb_bus_0.apb_3_pready( apb_3_pready );
    amba_apb_bus_0.apb_3_prdata( apb_3_prdata );
    amba_apb_bus_0.apb_4_pclk( apb_4_pclk );
    amba_apb_bus_0.apb_4_presetn( apb_4_presetn );
    amba_apb_bus_0.apb_4_paddr( apb_4_paddr );
    amba_apb_bus_0.apb_4_psel( apb_4_psel );
    amba_apb_bus_0.apb_4_penable( apb_4_penable );
    amba_apb_bus_0.apb_4_pwrite( apb_4_pwrite );
    amba_apb_bus_0.apb_4_pwdata( apb_4_pwdata );
    amba_apb_bus_0.apb_4_pready( apb_4_pready );
    amba_apb_bus_0.apb_4_prdata( apb_4_prdata );
    amba_apb_bus_0.apb_5_pclk( apb_5_pclk );
    amba_apb_bus_0.apb_5_presetn( apb_5_presetn );
    amba_apb_bus_0.apb_5_paddr( apb_5_paddr );
    amba_apb_bus_0.apb_5_psel( apb_5_psel );
    amba_apb_bus_0.apb_5_penable( apb_5_penable );
    amba_apb_bus_0.apb_5_pwrite( apb_5_pwrite );
    amba_apb_bus_0.apb_5_pwdata( apb_5_pwdata );
    amba_apb_bus_0.apb_5_pready( apb_5_pready );
    amba_apb_bus_0.apb_5_prdata( apb_5_prdata );
    amba_apb_bus_0.apb_6_pclk( apb_6_pclk );
    amba_apb_bus_0.apb_6_presetn( apb_6_presetn );
    amba_apb_bus_0.apb_6_paddr( apb_6_paddr );
    amba_apb_bus_0.apb_6_psel( apb_6_psel );
    amba_apb_bus_0.apb_6_penable( apb_6_penable );
    amba_apb_bus_0.apb_6_pwrite( apb_6_pwrite );
    amba_apb_bus_0.apb_6_pwdata( apb_6_pwdata );
    amba_apb_bus_0.apb_6_pready( apb_6_pready );
    amba_apb_bus_0.apb_6_prdata( apb_6_prdata );
    amba_apb_bus_0.apb_7_pclk( apb_7_pclk );
    amba_apb_bus_0.apb_7_presetn( apb_7_presetn );
    amba_apb_bus_0.apb_7_paddr( apb_7_paddr );
    amba_apb_bus_0.apb_7_psel( apb_7_psel );
    amba_apb_bus_0.apb_7_penable( apb_7_penable );
    amba_apb_bus_0.apb_7_pwrite( apb_7_pwrite );
    amba_apb_bus_0.apb_7_pwdata( apb_7_pwdata );
    amba_apb_bus_0.apb_7_pready( apb_7_pready );
    amba_apb_bus_0.apb_7_prdata( apb_7_prdata );
    amba_apb_bus_0.apb_8_pclk( apb_8_pclk );
    amba_apb_bus_0.apb_8_presetn( apb_8_presetn );
    amba_apb_bus_0.apb_8_paddr( apb_8_paddr );
    amba_apb_bus_0.apb_8_psel( apb_8_psel );
    amba_apb_bus_0.apb_8_penable( apb_8_penable );
    amba_apb_bus_0.apb_8_pwrite( apb_8_pwrite );
    amba_apb_bus_0.apb_8_pwdata( apb_8_pwdata );
    amba_apb_bus_0.apb_8_pready( apb_8_pready );
    amba_apb_bus_0.apb_8_prdata( apb_8_prdata );

    multiplier_0.pclk( apb_1_pclk );
    multiplier_0.presetn( apb_1_presetn );
    multiplier_0.paddr( apb_1_paddr );
    multiplier_0.psel( apb_1_psel );
    multiplier_0.penable( apb_1_penable );
    multiplier_0.pwrite( apb_1_pwrite );
    multiplier_0.pwdata( apb_1_pwdata );
    multiplier_0.pready( apb_1_pready );
    multiplier_0.prdata( apb_1_prdata );

    accelerometer_0.pclk( apb_2_pclk );
    accelerometer_0.presetn( apb_2_presetn );
    accelerometer_0.paddr( apb_2_paddr );
    accelerometer_0.psel( apb_2_psel );
    accelerometer_0.penable( apb_2_penable );
    accelerometer_0.pwrite( apb_2_pwrite );
    accelerometer_0.pwdata( apb_2_pwdata );
    accelerometer_0.pready( apb_2_pready );
    accelerometer_0.prdata( apb_2_prdata );
    accelerometer_0.analog_clock( apb_2_pclk );

    network_interface_0.pclk( apb_3_pclk );
    network_interface_0.presetn( apb_3_presetn );
    network_interface_0.paddr( apb_3_paddr );
    network_interface_0.psel( apb_3_psel );
    network_interface_0.penable( apb_3_penable );
    network_interface_0.pwrite( apb_3_pwrite );
    network_interface_0.pwdata( apb_3_pwdata );
    network_interface_0.pready( apb_3_pready );
    network_interface_0.prdata( apb_3_prdata );

    lfsr_0.pclk( apb_4_pclk );
    lfsr_0.presetn( apb_4_presetn );
    lfsr_0.paddr( apb_4_paddr );
    lfsr_0.psel( apb_4_psel );
    lfsr_0.penable( apb_4_penable );
    lfsr_0.pwrite( apb_4_pwrite );
    lfsr_0.pwdata( apb_4_pwdata );
    lfsr_0.pready( apb_4_pready );
    lfsr_0.prdata( apb_4_prdata );

    sensor_0.pclk( apb_5_pclk );
    sensor_0.presetn( apb_5_presetn );
    sensor_0.paddr( apb_5_paddr );
    sensor_0.psel( apb_5_psel );
    sensor_0.penable( apb_5_penable );
    sensor_0.pwrite( apb_5_pwrite );
    sensor_0.pwdata( apb_5_pwdata );
    sensor_0.pready( apb_5_pready );
    sensor_0.prdata( apb_5_prdata );
    sensor_0.sensor_high( sensor_high );
    sensor_0.sensor_low( sensor_low );

    actuator_0.pclk( apb_6_pclk );
    actuator_0.presetn( apb_6_presetn );
    actuator_0.paddr( apb_6_paddr );
    actuator_0.psel( apb_6_psel );
    actuator_0.penable( apb_6_penable );
    actuator_0.pwrite( apb_6_pwrite );
    actuator_0.pwdata( apb_6_pwdata );
    actuator_0.pready( apb_6_pready );
    actuator_0.prdata( apb_6_prdata );
    actuator_0.actuator_open( actuator_open );
    actuator_0.actuator_close( actuator_close );
    actuator_0.actuator_threshold( actuator_threshold );

    watchdog_0.pclk( apb_7_pclk );
    watchdog_0.presetn( apb_7_presetn );
    watchdog_0.paddr( apb_7_paddr );
    watchdog_0.psel( apb_7_psel );
    watchdog_0.penable( apb_7_penable );
    watchdog_0.pwrite( apb_7_pwrite );
    watchdog_0.pwdata( apb_7_pwdata );
    watchdog_0.pready( apb_7_pready );
    watchdog_0.prdata( apb_7_prdata );
}

complete_platform::~complete_platform()
{}



