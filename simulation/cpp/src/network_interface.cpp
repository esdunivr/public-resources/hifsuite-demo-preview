// /////////////////////////////////////////////////////////////////////////
// C++ code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#include "../inc/network_interface.hpp"



network_interface::network_interface() :
    tx_data_old(0UL),
    tx_data_ready_old(false),
    mode_old(false),
    reset_old(false),
    clk_old(false),
    hif_a2t_data(
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr),
    net_data_available(false),
    net_data_sent(false),
    net_data_incoming(0UL),
    net_data_outgoing(0UL),
    clkSuphif_2(false),
    net_data_available_new(false),
    net_data_sent_new(false),
    net_data_incoming_new(0UL),
    net_data_outgoing_new(0UL),
    clkSuphif_2_new(false),
    process_in_queue(false),
    flag_vhdl_process_UpdclkSuphif_2_executed(false),
    flag_clk(false)
{
}


network_interface::~network_interface()
{}


void network_interface::vhdl_process_UpdclkSuphif_2()
{
    if (*hif_a2t_data.clk != clkSuphif_2 && *hif_a2t_data.clk == true)
    {
        if (*hif_a2t_data.reset == true)
        {
            *hif_a2t_data.rx_data = 0UL;
            *hif_a2t_data.rx_data_ready = false;
            *hif_a2t_data.tx_data_done = false;
            net_data_outgoing_new = 0UL;
        }
        else
        {
            if (*hif_a2t_data.mode == false)
            {
                if (net_data_available == true)
                {
                    *hif_a2t_data.rx_data = net_data_incoming;
                    if (*hif_a2t_data.rx_data_ready == false)
                    {
                        *hif_a2t_data.rx_data_ready = true;
                    }
                }
                else
                {
                    *hif_a2t_data.rx_data = 0UL;
                    *hif_a2t_data.rx_data_ready = false;
                }
            }
            else
            {
                if (*hif_a2t_data.tx_data_ready == true)
                {
                    net_data_outgoing_new = *hif_a2t_data.tx_data;
                    *hif_a2t_data.tx_data_done = net_data_sent;
                }
                else
                {
                    net_data_outgoing_new = 0UL;
                    *hif_a2t_data.tx_data_done = false;
                }
            }
        }
    }
    clkSuphif_2_new = *hif_a2t_data.clk;
}


void network_interface::update_input_queue( bool synch_phase )
{
    process_in_queue = false;
    if (*hif_a2t_data.clk != clk_old)
    {
        clk_old = *hif_a2t_data.clk;
        flag_clk = true;
        process_in_queue = true;
    }
    else
    {
        flag_clk = false;
    }
    reset_old = *hif_a2t_data.reset;
    mode_old = *hif_a2t_data.mode;
    tx_data_ready_old = *hif_a2t_data.tx_data_ready;
    tx_data_old = *hif_a2t_data.tx_data;
}


void network_interface::update_event_queue()
{
    process_in_queue = false;
    update_input_queue( false );
    clkSuphif_2 = clkSuphif_2_new;
    net_data_available = net_data_available_new;
    net_data_incoming = net_data_incoming_new;
    net_data_outgoing = net_data_outgoing_new;
    net_data_sent = net_data_sent_new;
}


void network_interface::flag_elaboration()
{
    flag_vhdl_process_UpdclkSuphif_2_executed = false;
    if (flag_clk)
    {
        vhdl_process_UpdclkSuphif_2();
        flag_vhdl_process_UpdclkSuphif_2_executed = true;
    }
}


void network_interface::start_of_simulation()
{
    vhdl_process_UpdclkSuphif_2();
    flag_vhdl_process_UpdclkSuphif_2_executed = true;
}


void network_interface::initialize()
{
    start_of_simulation();
}




