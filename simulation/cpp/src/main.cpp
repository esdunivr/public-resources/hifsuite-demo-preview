#include "complete_platform.hpp"
#include "io_handler.hpp"

int main()
{
    // Create the platform.
    complete_platform platform_0;

    // Define variables used for simulation.
    complete_platform::complete_platform_iostruct io_exchange;
    int32_t cycles_number = 0;

    // Create an io handler.
    io_handler io_handler_0(&platform_0);
    // Load the ROM.
    io_handler_0.load_rom("../roms/rom.mem");

    // Initialize the platform.
    printf("Initializing...\n");
    platform_0.initialize();
    printf("Done\n");

    // Simulate.
    printf("Simulating...\n");
    while (!io_handler_0.status_exit())
    {
        // Simulate the platform.
        platform_0.simulate(&io_exchange, cycles_number);
        // Handle io.
        io_handler_0.io_process();
        // Change clock state.
        io_exchange.clk = !io_exchange.clk;
        // Update analog time.
        analogTime += 50E-09;
    }
    printf("Done\n");
    return 0;
}
