// /////////////////////////////////////////////////////////////////////////
// C++ code automatically generated by hif2sc
// Part of HIFSuite - Version stable
// Site: www.hifsuite.com - Contact: hifsuite@edalab.it
//
// HIFSuite copyright: EDALab s.r.l. - Networked Embedded Systems
// Site: www.edalab.it - Contact: info@edalab.it
// /////////////////////////////////////////////////////////////////////////


#include "../inc/amba_apb_bus.hpp"



amba_apb_bus::amba_apb_bus() :
    apb_8_prdata_old(0UL),
    apb_8_pready_old(false),
    apb_7_prdata_old(0UL),
    apb_7_pready_old(false),
    apb_6_prdata_old(0UL),
    apb_6_pready_old(false),
    apb_5_prdata_old(0UL),
    apb_5_pready_old(false),
    apb_4_prdata_old(0UL),
    apb_4_pready_old(false),
    apb_3_prdata_old(0UL),
    apb_3_pready_old(false),
    apb_2_prdata_old(0UL),
    apb_2_pready_old(false),
    apb_1_prdata_old(0UL),
    apb_1_pready_old(false),
    apb_master_pwdata_old(0UL),
    apb_master_pwrite_old(false),
    apb_master_penable_old(false),
    apb_master_psel8_old(false),
    apb_master_psel7_old(false),
    apb_master_psel6_old(false),
    apb_master_psel5_old(false),
    apb_master_psel4_old(false),
    apb_master_psel3_old(false),
    apb_master_psel2_old(false),
    apb_master_psel1_old(false),
    apb_master_paddr_old(0UL),
    apb_master_presetn_old(false),
    clk_old(false),
    hif_a2t_data(
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr),
    clkSuphif_0(false),
    clkSuphif_0_new(false),
    process_in_queue(false),
    flag_vhdl_process_UpdclkSuphif_0_executed(false),
    flag_clk(false)
{
}


amba_apb_bus::~amba_apb_bus()
{}


void amba_apb_bus::vhdl_process_UpdclkSuphif_0()
{
    *hif_a2t_data.apb_master_pclk = *hif_a2t_data.clk;
    *hif_a2t_data.apb_1_pclk = *hif_a2t_data.clk;
    *hif_a2t_data.apb_2_pclk = *hif_a2t_data.clk;
    *hif_a2t_data.apb_3_pclk = *hif_a2t_data.clk;
    *hif_a2t_data.apb_4_pclk = *hif_a2t_data.clk;
    *hif_a2t_data.apb_5_pclk = *hif_a2t_data.clk;
    *hif_a2t_data.apb_6_pclk = *hif_a2t_data.clk;
    *hif_a2t_data.apb_7_pclk = *hif_a2t_data.clk;
    *hif_a2t_data.apb_8_pclk = *hif_a2t_data.clk;
    if (*hif_a2t_data.clk != clkSuphif_0 && *hif_a2t_data.clk == true)
    {
        if (*hif_a2t_data.apb_master_psel1 == true)
        {
            *hif_a2t_data.apb_1_penable = *hif_a2t_data.apb_master_penable;
            *hif_a2t_data.apb_1_pwrite = *hif_a2t_data.apb_master_pwrite;
            *hif_a2t_data.apb_1_paddr = *hif_a2t_data.apb_master_paddr;
            *hif_a2t_data.apb_1_pwdata = *hif_a2t_data.apb_master_pwdata;
            *hif_a2t_data.apb_1_presetn = *hif_a2t_data.apb_master_presetn;
            *hif_a2t_data.apb_master_prdata = *hif_a2t_data.apb_1_prdata;
            *hif_a2t_data.apb_master_pready = *hif_a2t_data.apb_1_pready;
            *hif_a2t_data.apb_2_paddr = 0UL;
            *hif_a2t_data.apb_2_pwdata = 0UL;
            *hif_a2t_data.apb_2_penable = false;
            *hif_a2t_data.apb_2_pwrite = false;
            *hif_a2t_data.apb_2_presetn = false;
            *hif_a2t_data.apb_3_paddr = 0UL;
            *hif_a2t_data.apb_3_pwdata = 0UL;
            *hif_a2t_data.apb_3_penable = false;
            *hif_a2t_data.apb_3_pwrite = false;
            *hif_a2t_data.apb_3_presetn = false;
            *hif_a2t_data.apb_4_paddr = 0UL;
            *hif_a2t_data.apb_4_pwdata = 0UL;
            *hif_a2t_data.apb_4_penable = false;
            *hif_a2t_data.apb_4_pwrite = false;
            *hif_a2t_data.apb_4_presetn = false;
            *hif_a2t_data.apb_5_paddr = 0UL;
            *hif_a2t_data.apb_5_pwdata = 0UL;
            *hif_a2t_data.apb_5_penable = false;
            *hif_a2t_data.apb_5_pwrite = false;
            *hif_a2t_data.apb_5_presetn = false;
            *hif_a2t_data.apb_6_paddr = 0UL;
            *hif_a2t_data.apb_6_pwdata = 0UL;
            *hif_a2t_data.apb_6_penable = false;
            *hif_a2t_data.apb_6_pwrite = false;
            *hif_a2t_data.apb_6_presetn = false;
            *hif_a2t_data.apb_7_paddr = 0UL;
            *hif_a2t_data.apb_7_pwdata = 0UL;
            *hif_a2t_data.apb_7_penable = false;
            *hif_a2t_data.apb_7_pwrite = false;
            *hif_a2t_data.apb_7_presetn = false;
            *hif_a2t_data.apb_8_paddr = 0UL;
            *hif_a2t_data.apb_8_pwdata = 0UL;
            *hif_a2t_data.apb_8_penable = false;
            *hif_a2t_data.apb_8_pwrite = false;
            *hif_a2t_data.apb_8_presetn = false;
        }
        else if (*hif_a2t_data.apb_master_psel2 == true)
        {
            *hif_a2t_data.apb_2_penable = *hif_a2t_data.apb_master_penable;
            *hif_a2t_data.apb_2_pwrite = *hif_a2t_data.apb_master_pwrite;
            *hif_a2t_data.apb_2_paddr = *hif_a2t_data.apb_master_paddr;
            *hif_a2t_data.apb_2_pwdata = *hif_a2t_data.apb_master_pwdata;
            *hif_a2t_data.apb_2_presetn = *hif_a2t_data.apb_master_presetn;
            *hif_a2t_data.apb_master_prdata = *hif_a2t_data.apb_2_prdata;
            *hif_a2t_data.apb_master_pready = *hif_a2t_data.apb_2_pready;
            *hif_a2t_data.apb_1_paddr = 0UL;
            *hif_a2t_data.apb_1_pwdata = 0UL;
            *hif_a2t_data.apb_1_penable = false;
            *hif_a2t_data.apb_1_pwrite = false;
            *hif_a2t_data.apb_1_presetn = false;
            *hif_a2t_data.apb_3_paddr = 0UL;
            *hif_a2t_data.apb_3_pwdata = 0UL;
            *hif_a2t_data.apb_3_penable = false;
            *hif_a2t_data.apb_3_pwrite = false;
            *hif_a2t_data.apb_3_presetn = false;
            *hif_a2t_data.apb_4_paddr = 0UL;
            *hif_a2t_data.apb_4_pwdata = 0UL;
            *hif_a2t_data.apb_4_penable = false;
            *hif_a2t_data.apb_4_pwrite = false;
            *hif_a2t_data.apb_4_presetn = false;
            *hif_a2t_data.apb_5_paddr = 0UL;
            *hif_a2t_data.apb_5_pwdata = 0UL;
            *hif_a2t_data.apb_5_penable = false;
            *hif_a2t_data.apb_5_pwrite = false;
            *hif_a2t_data.apb_5_presetn = false;
            *hif_a2t_data.apb_6_paddr = 0UL;
            *hif_a2t_data.apb_6_pwdata = 0UL;
            *hif_a2t_data.apb_6_penable = false;
            *hif_a2t_data.apb_6_pwrite = false;
            *hif_a2t_data.apb_6_presetn = false;
            *hif_a2t_data.apb_7_paddr = 0UL;
            *hif_a2t_data.apb_7_pwdata = 0UL;
            *hif_a2t_data.apb_7_penable = false;
            *hif_a2t_data.apb_7_pwrite = false;
            *hif_a2t_data.apb_7_presetn = false;
            *hif_a2t_data.apb_8_paddr = 0UL;
            *hif_a2t_data.apb_8_pwdata = 0UL;
            *hif_a2t_data.apb_8_penable = false;
            *hif_a2t_data.apb_8_pwrite = false;
            *hif_a2t_data.apb_8_presetn = false;
        }
        else if (*hif_a2t_data.apb_master_psel3 == true)
        {
            *hif_a2t_data.apb_3_penable = *hif_a2t_data.apb_master_penable;
            *hif_a2t_data.apb_3_pwrite = *hif_a2t_data.apb_master_pwrite;
            *hif_a2t_data.apb_3_paddr = *hif_a2t_data.apb_master_paddr;
            *hif_a2t_data.apb_3_pwdata = *hif_a2t_data.apb_master_pwdata;
            *hif_a2t_data.apb_3_presetn = *hif_a2t_data.apb_master_presetn;
            *hif_a2t_data.apb_master_prdata = *hif_a2t_data.apb_3_prdata;
            *hif_a2t_data.apb_master_pready = *hif_a2t_data.apb_3_pready;
            *hif_a2t_data.apb_1_paddr = 0UL;
            *hif_a2t_data.apb_1_pwdata = 0UL;
            *hif_a2t_data.apb_1_penable = false;
            *hif_a2t_data.apb_1_pwrite = false;
            *hif_a2t_data.apb_1_presetn = false;
            *hif_a2t_data.apb_2_paddr = 0UL;
            *hif_a2t_data.apb_2_pwdata = 0UL;
            *hif_a2t_data.apb_2_penable = false;
            *hif_a2t_data.apb_2_pwrite = false;
            *hif_a2t_data.apb_2_presetn = false;
            *hif_a2t_data.apb_4_paddr = 0UL;
            *hif_a2t_data.apb_4_pwdata = 0UL;
            *hif_a2t_data.apb_4_penable = false;
            *hif_a2t_data.apb_4_pwrite = false;
            *hif_a2t_data.apb_4_presetn = false;
            *hif_a2t_data.apb_5_paddr = 0UL;
            *hif_a2t_data.apb_5_pwdata = 0UL;
            *hif_a2t_data.apb_5_penable = false;
            *hif_a2t_data.apb_5_pwrite = false;
            *hif_a2t_data.apb_5_presetn = false;
            *hif_a2t_data.apb_6_paddr = 0UL;
            *hif_a2t_data.apb_6_pwdata = 0UL;
            *hif_a2t_data.apb_6_penable = false;
            *hif_a2t_data.apb_6_pwrite = false;
            *hif_a2t_data.apb_6_presetn = false;
            *hif_a2t_data.apb_7_paddr = 0UL;
            *hif_a2t_data.apb_7_pwdata = 0UL;
            *hif_a2t_data.apb_7_penable = false;
            *hif_a2t_data.apb_7_pwrite = false;
            *hif_a2t_data.apb_7_presetn = false;
            *hif_a2t_data.apb_8_paddr = 0UL;
            *hif_a2t_data.apb_8_pwdata = 0UL;
            *hif_a2t_data.apb_8_penable = false;
            *hif_a2t_data.apb_8_pwrite = false;
            *hif_a2t_data.apb_8_presetn = false;
        }
        else if (*hif_a2t_data.apb_master_psel4 == true)
        {
            *hif_a2t_data.apb_4_penable = *hif_a2t_data.apb_master_penable;
            *hif_a2t_data.apb_4_pwrite = *hif_a2t_data.apb_master_pwrite;
            *hif_a2t_data.apb_4_paddr = *hif_a2t_data.apb_master_paddr;
            *hif_a2t_data.apb_4_pwdata = *hif_a2t_data.apb_master_pwdata;
            *hif_a2t_data.apb_4_presetn = *hif_a2t_data.apb_master_presetn;
            *hif_a2t_data.apb_master_prdata = *hif_a2t_data.apb_4_prdata;
            *hif_a2t_data.apb_master_pready = *hif_a2t_data.apb_4_pready;
            *hif_a2t_data.apb_1_paddr = 0UL;
            *hif_a2t_data.apb_1_pwdata = 0UL;
            *hif_a2t_data.apb_1_penable = false;
            *hif_a2t_data.apb_1_pwrite = false;
            *hif_a2t_data.apb_1_presetn = false;
            *hif_a2t_data.apb_2_paddr = 0UL;
            *hif_a2t_data.apb_2_pwdata = 0UL;
            *hif_a2t_data.apb_2_penable = false;
            *hif_a2t_data.apb_2_pwrite = false;
            *hif_a2t_data.apb_2_presetn = false;
            *hif_a2t_data.apb_3_paddr = 0UL;
            *hif_a2t_data.apb_3_pwdata = 0UL;
            *hif_a2t_data.apb_3_penable = false;
            *hif_a2t_data.apb_3_pwrite = false;
            *hif_a2t_data.apb_3_presetn = false;
            *hif_a2t_data.apb_5_paddr = 0UL;
            *hif_a2t_data.apb_5_pwdata = 0UL;
            *hif_a2t_data.apb_5_penable = false;
            *hif_a2t_data.apb_5_pwrite = false;
            *hif_a2t_data.apb_5_presetn = false;
            *hif_a2t_data.apb_6_paddr = 0UL;
            *hif_a2t_data.apb_6_pwdata = 0UL;
            *hif_a2t_data.apb_6_penable = false;
            *hif_a2t_data.apb_6_pwrite = false;
            *hif_a2t_data.apb_6_presetn = false;
            *hif_a2t_data.apb_7_paddr = 0UL;
            *hif_a2t_data.apb_7_pwdata = 0UL;
            *hif_a2t_data.apb_7_penable = false;
            *hif_a2t_data.apb_7_pwrite = false;
            *hif_a2t_data.apb_7_presetn = false;
            *hif_a2t_data.apb_8_paddr = 0UL;
            *hif_a2t_data.apb_8_pwdata = 0UL;
            *hif_a2t_data.apb_8_penable = false;
            *hif_a2t_data.apb_8_pwrite = false;
            *hif_a2t_data.apb_8_presetn = false;
        }
        else if (*hif_a2t_data.apb_master_psel5 == true)
        {
            *hif_a2t_data.apb_5_penable = *hif_a2t_data.apb_master_penable;
            *hif_a2t_data.apb_5_pwrite = *hif_a2t_data.apb_master_pwrite;
            *hif_a2t_data.apb_5_paddr = *hif_a2t_data.apb_master_paddr;
            *hif_a2t_data.apb_5_pwdata = *hif_a2t_data.apb_master_pwdata;
            *hif_a2t_data.apb_5_presetn = *hif_a2t_data.apb_master_presetn;
            *hif_a2t_data.apb_master_prdata = *hif_a2t_data.apb_5_prdata;
            *hif_a2t_data.apb_master_pready = *hif_a2t_data.apb_5_pready;
            *hif_a2t_data.apb_1_paddr = 0UL;
            *hif_a2t_data.apb_1_pwdata = 0UL;
            *hif_a2t_data.apb_1_penable = false;
            *hif_a2t_data.apb_1_pwrite = false;
            *hif_a2t_data.apb_1_presetn = false;
            *hif_a2t_data.apb_2_paddr = 0UL;
            *hif_a2t_data.apb_2_pwdata = 0UL;
            *hif_a2t_data.apb_2_penable = false;
            *hif_a2t_data.apb_2_pwrite = false;
            *hif_a2t_data.apb_2_presetn = false;
            *hif_a2t_data.apb_3_paddr = 0UL;
            *hif_a2t_data.apb_3_pwdata = 0UL;
            *hif_a2t_data.apb_3_penable = false;
            *hif_a2t_data.apb_3_pwrite = false;
            *hif_a2t_data.apb_3_presetn = false;
            *hif_a2t_data.apb_4_paddr = 0UL;
            *hif_a2t_data.apb_4_pwdata = 0UL;
            *hif_a2t_data.apb_4_penable = false;
            *hif_a2t_data.apb_4_pwrite = false;
            *hif_a2t_data.apb_4_presetn = false;
            *hif_a2t_data.apb_6_paddr = 0UL;
            *hif_a2t_data.apb_6_pwdata = 0UL;
            *hif_a2t_data.apb_6_penable = false;
            *hif_a2t_data.apb_6_pwrite = false;
            *hif_a2t_data.apb_6_presetn = false;
            *hif_a2t_data.apb_7_paddr = 0UL;
            *hif_a2t_data.apb_7_pwdata = 0UL;
            *hif_a2t_data.apb_7_penable = false;
            *hif_a2t_data.apb_7_pwrite = false;
            *hif_a2t_data.apb_7_presetn = false;
            *hif_a2t_data.apb_8_paddr = 0UL;
            *hif_a2t_data.apb_8_pwdata = 0UL;
            *hif_a2t_data.apb_8_penable = false;
            *hif_a2t_data.apb_8_pwrite = false;
            *hif_a2t_data.apb_8_presetn = false;
        }
        else if (*hif_a2t_data.apb_master_psel6 == true)
        {
            *hif_a2t_data.apb_6_penable = *hif_a2t_data.apb_master_penable;
            *hif_a2t_data.apb_6_pwrite = *hif_a2t_data.apb_master_pwrite;
            *hif_a2t_data.apb_6_paddr = *hif_a2t_data.apb_master_paddr;
            *hif_a2t_data.apb_6_pwdata = *hif_a2t_data.apb_master_pwdata;
            *hif_a2t_data.apb_6_presetn = *hif_a2t_data.apb_master_presetn;
            *hif_a2t_data.apb_master_prdata = *hif_a2t_data.apb_6_prdata;
            *hif_a2t_data.apb_master_pready = *hif_a2t_data.apb_6_pready;
            *hif_a2t_data.apb_1_paddr = 0UL;
            *hif_a2t_data.apb_1_pwdata = 0UL;
            *hif_a2t_data.apb_1_penable = false;
            *hif_a2t_data.apb_1_pwrite = false;
            *hif_a2t_data.apb_1_presetn = false;
            *hif_a2t_data.apb_2_paddr = 0UL;
            *hif_a2t_data.apb_2_pwdata = 0UL;
            *hif_a2t_data.apb_2_penable = false;
            *hif_a2t_data.apb_2_pwrite = false;
            *hif_a2t_data.apb_2_presetn = false;
            *hif_a2t_data.apb_3_paddr = 0UL;
            *hif_a2t_data.apb_3_pwdata = 0UL;
            *hif_a2t_data.apb_3_penable = false;
            *hif_a2t_data.apb_3_pwrite = false;
            *hif_a2t_data.apb_3_presetn = false;
            *hif_a2t_data.apb_4_paddr = 0UL;
            *hif_a2t_data.apb_4_pwdata = 0UL;
            *hif_a2t_data.apb_4_penable = false;
            *hif_a2t_data.apb_4_pwrite = false;
            *hif_a2t_data.apb_4_presetn = false;
            *hif_a2t_data.apb_5_paddr = 0UL;
            *hif_a2t_data.apb_5_pwdata = 0UL;
            *hif_a2t_data.apb_5_penable = false;
            *hif_a2t_data.apb_5_pwrite = false;
            *hif_a2t_data.apb_5_presetn = false;
            *hif_a2t_data.apb_7_paddr = 0UL;
            *hif_a2t_data.apb_7_pwdata = 0UL;
            *hif_a2t_data.apb_7_penable = false;
            *hif_a2t_data.apb_7_pwrite = false;
            *hif_a2t_data.apb_7_presetn = false;
            *hif_a2t_data.apb_8_paddr = 0UL;
            *hif_a2t_data.apb_8_pwdata = 0UL;
            *hif_a2t_data.apb_8_penable = false;
            *hif_a2t_data.apb_8_pwrite = false;
            *hif_a2t_data.apb_8_presetn = false;
        }
        else if (*hif_a2t_data.apb_master_psel7 == true)
        {
            *hif_a2t_data.apb_7_penable = *hif_a2t_data.apb_master_penable;
            *hif_a2t_data.apb_7_pwrite = *hif_a2t_data.apb_master_pwrite;
            *hif_a2t_data.apb_7_paddr = *hif_a2t_data.apb_master_paddr;
            *hif_a2t_data.apb_7_pwdata = *hif_a2t_data.apb_master_pwdata;
            *hif_a2t_data.apb_7_presetn = *hif_a2t_data.apb_master_presetn;
            *hif_a2t_data.apb_master_prdata = *hif_a2t_data.apb_7_prdata;
            *hif_a2t_data.apb_master_pready = *hif_a2t_data.apb_7_pready;
            *hif_a2t_data.apb_1_paddr = 0UL;
            *hif_a2t_data.apb_1_pwdata = 0UL;
            *hif_a2t_data.apb_1_penable = false;
            *hif_a2t_data.apb_1_pwrite = false;
            *hif_a2t_data.apb_1_presetn = false;
            *hif_a2t_data.apb_2_paddr = 0UL;
            *hif_a2t_data.apb_2_pwdata = 0UL;
            *hif_a2t_data.apb_2_penable = false;
            *hif_a2t_data.apb_2_pwrite = false;
            *hif_a2t_data.apb_2_presetn = false;
            *hif_a2t_data.apb_3_paddr = 0UL;
            *hif_a2t_data.apb_3_pwdata = 0UL;
            *hif_a2t_data.apb_3_penable = false;
            *hif_a2t_data.apb_3_pwrite = false;
            *hif_a2t_data.apb_3_presetn = false;
            *hif_a2t_data.apb_4_paddr = 0UL;
            *hif_a2t_data.apb_4_pwdata = 0UL;
            *hif_a2t_data.apb_4_penable = false;
            *hif_a2t_data.apb_4_pwrite = false;
            *hif_a2t_data.apb_4_presetn = false;
            *hif_a2t_data.apb_5_paddr = 0UL;
            *hif_a2t_data.apb_5_pwdata = 0UL;
            *hif_a2t_data.apb_5_penable = false;
            *hif_a2t_data.apb_5_pwrite = false;
            *hif_a2t_data.apb_5_presetn = false;
            *hif_a2t_data.apb_6_paddr = 0UL;
            *hif_a2t_data.apb_6_pwdata = 0UL;
            *hif_a2t_data.apb_6_penable = false;
            *hif_a2t_data.apb_6_pwrite = false;
            *hif_a2t_data.apb_6_presetn = false;
            *hif_a2t_data.apb_8_paddr = 0UL;
            *hif_a2t_data.apb_8_pwdata = 0UL;
            *hif_a2t_data.apb_8_penable = false;
            *hif_a2t_data.apb_8_pwrite = false;
            *hif_a2t_data.apb_8_presetn = false;
        }
        else if (*hif_a2t_data.apb_master_psel8 == true)
        {
            *hif_a2t_data.apb_8_penable = *hif_a2t_data.apb_master_penable;
            *hif_a2t_data.apb_8_pwrite = *hif_a2t_data.apb_master_pwrite;
            *hif_a2t_data.apb_8_paddr = *hif_a2t_data.apb_master_paddr;
            *hif_a2t_data.apb_8_pwdata = *hif_a2t_data.apb_master_pwdata;
            *hif_a2t_data.apb_8_presetn = *hif_a2t_data.apb_master_presetn;
            *hif_a2t_data.apb_master_prdata = *hif_a2t_data.apb_8_prdata;
            *hif_a2t_data.apb_master_pready = *hif_a2t_data.apb_8_pready;
            *hif_a2t_data.apb_1_paddr = 0UL;
            *hif_a2t_data.apb_1_pwdata = 0UL;
            *hif_a2t_data.apb_1_penable = false;
            *hif_a2t_data.apb_1_pwrite = false;
            *hif_a2t_data.apb_1_presetn = false;
            *hif_a2t_data.apb_2_paddr = 0UL;
            *hif_a2t_data.apb_2_pwdata = 0UL;
            *hif_a2t_data.apb_2_penable = false;
            *hif_a2t_data.apb_2_pwrite = false;
            *hif_a2t_data.apb_2_presetn = false;
            *hif_a2t_data.apb_3_paddr = 0UL;
            *hif_a2t_data.apb_3_pwdata = 0UL;
            *hif_a2t_data.apb_3_penable = false;
            *hif_a2t_data.apb_3_pwrite = false;
            *hif_a2t_data.apb_3_presetn = false;
            *hif_a2t_data.apb_4_paddr = 0UL;
            *hif_a2t_data.apb_4_pwdata = 0UL;
            *hif_a2t_data.apb_4_penable = false;
            *hif_a2t_data.apb_4_pwrite = false;
            *hif_a2t_data.apb_4_presetn = false;
            *hif_a2t_data.apb_5_paddr = 0UL;
            *hif_a2t_data.apb_5_pwdata = 0UL;
            *hif_a2t_data.apb_5_penable = false;
            *hif_a2t_data.apb_5_pwrite = false;
            *hif_a2t_data.apb_5_presetn = false;
            *hif_a2t_data.apb_6_paddr = 0UL;
            *hif_a2t_data.apb_6_pwdata = 0UL;
            *hif_a2t_data.apb_6_penable = false;
            *hif_a2t_data.apb_6_pwrite = false;
            *hif_a2t_data.apb_6_presetn = false;
            *hif_a2t_data.apb_7_paddr = 0UL;
            *hif_a2t_data.apb_7_pwdata = 0UL;
            *hif_a2t_data.apb_7_penable = false;
            *hif_a2t_data.apb_7_pwrite = false;
            *hif_a2t_data.apb_7_presetn = false;
        }
        *hif_a2t_data.apb_1_psel = *hif_a2t_data.apb_master_psel1;
        *hif_a2t_data.apb_2_psel = *hif_a2t_data.apb_master_psel2;
        *hif_a2t_data.apb_3_psel = *hif_a2t_data.apb_master_psel3;
        *hif_a2t_data.apb_4_psel = *hif_a2t_data.apb_master_psel4;
        *hif_a2t_data.apb_5_psel = *hif_a2t_data.apb_master_psel5;
        *hif_a2t_data.apb_6_psel = *hif_a2t_data.apb_master_psel6;
        *hif_a2t_data.apb_7_psel = *hif_a2t_data.apb_master_psel7;
        *hif_a2t_data.apb_8_psel = *hif_a2t_data.apb_master_psel8;
    }
    clkSuphif_0_new = *hif_a2t_data.clk;
}


void amba_apb_bus::update_input_queue( bool synch_phase )
{
    process_in_queue = false;
    if (*hif_a2t_data.clk != clk_old)
    {
        clk_old = *hif_a2t_data.clk;
        flag_clk = true;
        process_in_queue = true;
    }
    else
    {
        flag_clk = false;
    }
    apb_master_presetn_old = *hif_a2t_data.apb_master_presetn;
    apb_master_paddr_old = *hif_a2t_data.apb_master_paddr;
    apb_master_psel1_old = *hif_a2t_data.apb_master_psel1;
    apb_master_psel2_old = *hif_a2t_data.apb_master_psel2;
    apb_master_psel3_old = *hif_a2t_data.apb_master_psel3;
    apb_master_psel4_old = *hif_a2t_data.apb_master_psel4;
    apb_master_psel5_old = *hif_a2t_data.apb_master_psel5;
    apb_master_psel6_old = *hif_a2t_data.apb_master_psel6;
    apb_master_psel7_old = *hif_a2t_data.apb_master_psel7;
    apb_master_psel8_old = *hif_a2t_data.apb_master_psel8;
    apb_master_penable_old = *hif_a2t_data.apb_master_penable;
    apb_master_pwrite_old = *hif_a2t_data.apb_master_pwrite;
    apb_master_pwdata_old = *hif_a2t_data.apb_master_pwdata;
    apb_1_pready_old = *hif_a2t_data.apb_1_pready;
    apb_1_prdata_old = *hif_a2t_data.apb_1_prdata;
    apb_2_pready_old = *hif_a2t_data.apb_2_pready;
    apb_2_prdata_old = *hif_a2t_data.apb_2_prdata;
    apb_3_pready_old = *hif_a2t_data.apb_3_pready;
    apb_3_prdata_old = *hif_a2t_data.apb_3_prdata;
    apb_4_pready_old = *hif_a2t_data.apb_4_pready;
    apb_4_prdata_old = *hif_a2t_data.apb_4_prdata;
    apb_5_pready_old = *hif_a2t_data.apb_5_pready;
    apb_5_prdata_old = *hif_a2t_data.apb_5_prdata;
    apb_6_pready_old = *hif_a2t_data.apb_6_pready;
    apb_6_prdata_old = *hif_a2t_data.apb_6_prdata;
    apb_7_pready_old = *hif_a2t_data.apb_7_pready;
    apb_7_prdata_old = *hif_a2t_data.apb_7_prdata;
    apb_8_pready_old = *hif_a2t_data.apb_8_pready;
    apb_8_prdata_old = *hif_a2t_data.apb_8_prdata;
}


void amba_apb_bus::update_event_queue()
{
    process_in_queue = false;
    update_input_queue( false );
    clkSuphif_0 = clkSuphif_0_new;
}


void amba_apb_bus::flag_elaboration()
{
    flag_vhdl_process_UpdclkSuphif_0_executed = false;
    if (flag_clk)
    {
        vhdl_process_UpdclkSuphif_0();
        flag_vhdl_process_UpdclkSuphif_0_executed = true;
    }
}


void amba_apb_bus::start_of_simulation()
{
    vhdl_process_UpdclkSuphif_0();
    flag_vhdl_process_UpdclkSuphif_0_executed = true;
}


void amba_apb_bus::initialize()
{
    start_of_simulation();
}




