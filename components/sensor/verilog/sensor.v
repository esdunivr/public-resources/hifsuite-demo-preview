// ----------------------------------------------------------
// Design   : Sensor
// Author   : Stefano Centomo
// Modified : 15/06/2016
// ----------------------------------------------------------

module sensor(
    // Amba Interface
    pclk,
    presetn,
    paddr,
    psel,
    penable,
    pwrite,
    pwdata,
    pready,
    prdata,
    // Sensor Interface
    sensor_high,
    sensor_low
    );
    // ------------------------------------------------------------------------
    // PORTS
    // ------------------------------------------------------------------------
    // Bus Interface
    input             pclk;
    input             presetn;
    input [31:0]      paddr;
    input             psel;
    input             penable;
    input             pwrite;
    input [31:0]      pwdata;
    output reg        pready;
    output reg [31:0] prdata;

    //Actuator Interface
    input         sensor_high;
    input         sensor_low;
    
    
    reg         sensor_high_reg;
    reg         sensor_low_reg;

    
    always @(posedge pclk) begin

        // Update the values only when new values income
        if(penable) begin

            prdata[0]    <= sensor_high;
            prdata[7:1]  <= 7'b0;

            prdata[8]    <= sensor_low;
            prdata[15:9] <= 7'b0;

            prdata[31:16] <= 16'b0;


            pready <= 1'b1;
            
        end
        else begin
            // set everything at zero
            prdata[31:0] <= 32'b0;
            pready <= 1'b0;
        
        end  
    
    end
endmodule