// ----------------------------------------------------------
// Design   : Watchdog Counter
// Author   : Enrico Fraccaroli, Stefano Centomo
// Modified : 06 Feb 2019
// Description :
// A Simple Counter driven by the inputs. When start_counting is set it starts 
// counting the number of clock period. 
// If the counter(actual_counter) crosses max_counter, counter_flag is set.
// 
// 
// 
// 
// 
// ----------------------------------------------------------
`define COUNTER_OFFSET 16'd1260
module watchdog(
	clk,
	start_counting,
	stop_counting,
	max_counter,
	counter_flag,
	actual_counter,
	dout_rdy
	);



	// this represents the value of 
    
    localparam st_1				= 3'b000;
    localparam st_2				= 3'b001;
    localparam st_3				= 3'b010;
    localparam st_4				= 3'b011;
    localparam st_5				= 3'b100;

	input 			clk;
    input 	[15:0] 	max_counter;
    input 			start_counting;
	input 			stop_counting;

	output  reg [15:0] 	actual_counter;
    output  reg 		counter_flag;
    output  reg 		dout_rdy;

	reg[2:0] state,next_state;
	reg[31:0] internal_max_counter;

initial begin
	state = st_1;
	next_state <= st_1;
	actual_counter<= 16'h0000;
	counter_flag <= 1'b0;

end 

always@(state, start_counting, stop_counting) begin

    case(state)

    	st_1: begin
    		
    		if(start_counting == 1'b1) begin
    			next_state <= st_2;
    		end
		
		end 

    	st_2: begin
    	
    		if(stop_counting == 1'b1) begin
    			next_state <= st_3;
    		end
    	end

    	st_3: begin

    		next_state <= st_4;
    	end

    	st_4: begin
    		if(start_counting == 1'b1) begin
    			next_state <= st_5;
    		end
    	end

    	st_5: begin
    			next_state <= st_2; 
    	end

    endcase 
end


always @(posedge clk) begin
	
	state <= next_state;

	case(next_state) 
	
		st_1: begin
			internal_max_counter <= max_counter;
			actual_counter	<= 16'h0000;
			counter_flag 	<= 1'b0;
			dout_rdy <= 1'b0;

		end 

		st_2: begin

			// m6502 is sensible on each clock edges. 
			// This module is sentitive only on posedge
			actual_counter <= actual_counter + 2; 

			if((actual_counter) >= (internal_max_counter +`COUNTER_OFFSET)) begin
				counter_flag <= 1'b1;
			end

			dout_rdy <= 1'b0;
		end

		st_3: begin
			actual_counter <= actual_counter-`COUNTER_OFFSET;
			dout_rdy <= 1'b0;
		end

		st_4: begin

			dout_rdy <= 1'b1;
			internal_max_counter <= max_counter;
		end

		st_5: begin
			
			actual_counter 	<= 16'h0000;
			counter_flag 	<= 1'b0;
			dout_rdy <= 1'b0;


		end

		
	endcase // state

end 

endmodule
