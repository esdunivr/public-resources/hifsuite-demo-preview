// ----------------------------------------------------------
// Design   : Wrapper for a Watchdog.
// Author   : Stefano Centomo
// Modified : 06 Feb 2019
// ----------------------------------------------------------

module watchdog_apb_wrapper(
    pclk,
    presetn,
    paddr,
    psel,
    penable,
    pwrite,
    pwdata,
    pready,
    prdata);
    // ------------------------------------------------------------------------
    // PORTS
    // ------------------------------------------------------------------------
    input             pclk;
    input             presetn;
    input [31:0]      paddr;
    input             psel;
    input             penable;
    input             pwrite;
    input [31:0]      pwdata;
    output wire        pready;
    output wire [31:0] prdata;

    wire clk;
    wire start_counting;
    wire stop_counting;
    wire [15:0] max_counter;
    wire [15:0] actual_counter;
    wire counter_flag;
    wire dout_rdy;
    // ------------------------------------------------------------------------
    // SUB CIRCUITS
    // ------------------------------------------------------------------------


    //always@(posedge pclk) begin
    assign    clk             = pclk;
    assign    stop_counting   = pwdata[24];
    assign    start_counting  = pwdata[16];
    assign    max_counter     = pwdata[15:0];

    assign    prdata[15:0]    = actual_counter;
    assign    prdata[16]      = counter_flag;
    assign    prdata[31:17]   = 17'b00000000000000000;
    assign    pready          = dout_rdy;
    //end

    watchdog watchdog_0(
                        .clk(pclk), 
                        .start_counting(start_counting), 
                        .stop_counting(stop_counting), 
                        .max_counter(max_counter),
                        .actual_counter(actual_counter),
                        .counter_flag(counter_flag),
                        .dout_rdy(dout_rdy)
                        );
endmodule