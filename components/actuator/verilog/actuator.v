// ----------------------------------------------------------
// Design   : Actuator
// Author   : Stefano Centomo
// Modified : 15/06/2016
// ----------------------------------------------------------

module actuator(
    // Amba Interface
    pclk,
    presetn,
    paddr,
    psel,
    penable,
    pwrite,
    pwdata,
    pready,
    prdata,
    // Actuator Interface
    actuator_open,
    actuator_close,
    actuator_threshold
    );
    // ------------------------------------------------------------------------
    // PORTS
    // ------------------------------------------------------------------------
    // Bus Interface
    input             pclk;
    input             presetn;
    input [31:0]      paddr;
    input             psel;
    input             penable;
    input             pwrite;
    input [31:0]      pwdata;
    output reg        pready;
    output reg [31:0] prdata;

    //Actuator Interface
    output reg        actuator_open;
    output reg        actuator_close;
    output reg [15:0] actuator_threshold;
    

    reg         actuator_open_reg;
    reg         actuator_close_reg;
    reg [15:0]  actuator_threshold_reg;

    initial begin 
        
        actuator_open           <= 1'b0;
        actuator_close          <= 1'b0;
        actuator_threshold      <= 16'b0;

        actuator_open_reg       <= 1'b0;
        actuator_close_reg      <= 1'b0;
        actuator_threshold_reg  <= 16'b0;

    end
    always @(posedge pclk) begin

        actuator_open        <= actuator_open_reg;
        actuator_close       <= actuator_close_reg;
        actuator_threshold   <= actuator_threshold_reg;

        if(penable) begin
            actuator_open_reg       <= pwdata[0];
            actuator_close_reg      <= pwdata[8];
            actuator_threshold_reg  <= pwdata[31:16];
            pready <= 1'b1;
        end else begin
            pready <= 1'b0;
        end
                
            
            //pready <= 1'b0;
    end
endmodule