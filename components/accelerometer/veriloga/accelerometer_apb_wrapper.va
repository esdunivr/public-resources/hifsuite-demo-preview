// ----------------------------------------------------------
// @brief Accelerometer and ADC.
// @author Enrico Fraccaroli
// @date   12 Apr 2018
// ----------------------------------------------------------

`include "constants.vams"
`include "disciplines.vams"

module accelerometer_apb_wrapper(
    pclk,
    presetn,
    paddr,
    psel,
    penable,
    pwrite,
    pwdata,
    pready,
    prdata);
    
    // ------------------------------------------------------------------------
    // PORTS
    // ------------------------------------------------------------------------
    input             pclk;
    input             presetn;
    input [31:0]      paddr;
    input             psel;
    input             penable;
    input             pwrite;
    input [31:0]      pwdata;
    output reg        pready;
    output reg [31:0] prdata;

	// ------------------------------------------------------------------------
	// ANALOG AND LOGIC NODES
	// ------------------------------------------------------------------------
    electrical avz, avy, avx, gnd;
    ground gnd;
	wire [8-1:0] cap7_d;
	wire [8-1:0] cap5_d;
	wire [8-1:0] cap2_d;
	wire [8-1:0] cap1_d;

    // ------------------------------------------------------------------------
    // SUB CIRCUITS
    // ------------------------------------------------------------------------
    accelerometer acc(
    	.clk(pclk),
		.cap7_d(cap7_d), .cap5_d(cap5_d),
		.cap2_d(cap2_d), .cap1_d(cap1_d),
		.avx(avx), .avy(avy), .avz(avz),
        .gnd(gnd));

    // ------------------------------------------------------------------------
    // DIGITAL BEHAVIOUR (BUS INTERFACE)
    // ------------------------------------------------------------------------
    always @(posedge pclk) begin
		// If pwrite is zero wrapper is in READ mode, otherwise WRITE mode.
		if (penable == 1'b1) begin
            prdata[31:24] <= cap7_d;
            prdata[23:16] <= cap5_d;
            prdata[15:8]  <= cap2_d;
            prdata[7:0]   <= cap1_d;
            pready <= 1'd1;
		end
        else begin
            prdata = 32'd0;
            pready =  1'b0;
		end
    end

    // ------------------------------------------------------------------------
    // ANALOG BEHAVIOUR
    // ------------------------------------------------------------------------
    analog begin
        V(avx, gnd) <+ 3.3 * sin(1.15 * `M_TWO_PI * $abstime);
        V(avy, gnd) <+ 1.65 * sin(3.25 * `M_TWO_PI * $abstime);
        V(avz, gnd) <+ 2.56 * sin(9.81 * `M_TWO_PI * $abstime);
    end
endmodule