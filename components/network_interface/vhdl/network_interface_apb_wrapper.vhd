----------------------------------------------------------
-- Design   : APB Slave Wrapper for a simple network interface
-- Author   : Enrico Fraccaroli
-- Modified : 16 May 2018
----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY network_interface_apb_wrapper IS
	PORT (
		pclk	: in std_logic;
		presetn : in std_logic;
		paddr 	: in std_logic_vector(31 downto 0);
		psel 	: in std_logic;
		penable : in std_logic;
		pwrite 	: in std_logic;
		pwdata 	: in std_logic_vector(31 downto 0);
		pready 	: out std_logic;
		prdata 	: out std_logic_vector(31 downto 0)
	);
END network_interface_apb_wrapper;

ARCHITECTURE network_interface_apb_wrapper OF network_interface_apb_wrapper IS
	COMPONENT network_interface
		PORT (
			clk			    	: in 	std_logic;
			reset  		    	: in 	std_logic;
			-- MODE SELECTION (0: receive, 1: send)
			mode 				: in	std_logic;
			-- RECEIVER
			rx_data_ready      	: out 	std_logic;
			rx_data		      	: out 	std_logic_vector( 31 downto 0 );
			-- TRANSMITTER
	        tx_data_ready     	: in  	std_logic;
			tx_data		    	: in  	std_logic_vector( 31 downto 0 );
	        tx_data_done    	: out 	std_logic
		);
	END COMPONENT network_interface;
	SIGNAL clk			    	: std_logic;
	SIGNAL reset  		    	: std_logic;
	-- MODE SELECTION (0: receive, 1: send)
	SIGNAL mode 				: std_logic;
	-- RECEIVER
	SIGNAL rx_data_ready      	: std_logic;
	SIGNAL rx_data		      	: std_logic_vector( 31 downto 0 );
	-- TRANSMITTER
	SIGNAL tx_data_ready     	: std_logic;
	SIGNAL tx_data		    	: std_logic_vector( 31 downto 0 );
	SIGNAL tx_data_done    	    : std_logic;
BEGIN
	-- ------------------------------------------------------------------------
	-- From BUS to network_interface
	-- ------------------------------------------------------------------------
	clk    <= pclk;
	reset  <= presetn;
	mode   <= pwrite;

	pready <= tx_data_done or rx_data_ready;
	prdata <= rx_data;

	tx_data_ready <= penable;
	tx_data <= pwdata;

	-- ------------------------------------------------------------------------
	-- MAPPING
	-- ------------------------------------------------------------------------
	network_interface_0 : network_interface
	PORT MAP(
		clk 	=> clk,
		reset 	=> reset,
		mode 	=> mode,
		rx_data_ready => rx_data_ready,
		rx_data => rx_data,
		tx_data_ready => tx_data_ready,
		tx_data => tx_data,
		tx_data_done => tx_data_done
	);
END network_interface_apb_wrapper;
