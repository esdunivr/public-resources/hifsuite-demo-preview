----------------------------------------------------------
-- Design   : Simple Metwork Interface
-- Author   : Enrico Fraccaroli
-- Modified : 16 May 2018
----------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY network_interface IS
	PORT (
		clk			    	: in 	std_logic;
		reset  		    	: in 	std_logic;
		-- MODE SELECTION (0: receive, 1: send)
		mode 				: in	std_logic;
		-- RECEIVER
		rx_data_ready      	: out 	std_logic;
		rx_data		      	: out 	std_logic_vector( 31 downto 0 );
		-- TRANSMITTER
        tx_data_ready     	: in  	std_logic;
		tx_data		    	: in  	std_logic_vector( 31 downto 0 );
        tx_data_done    	: out 	std_logic
	);
END network_interface;

ARCHITECTURE network_interface OF network_interface IS
-- TO AND FROM NETWORK
SIGNAL net_data_available  : std_logic;
SIGNAL net_data_sent       : std_logic;
SIGNAL net_data_incoming   : std_logic_vector( 31 downto 0 );
SIGNAL net_data_outgoing   : std_logic_vector( 31 downto 0 );
BEGIN
	PROCESS (clk)
	BEGIN
        IF ( clk'event and clk = '1' ) THEN
			-- RESET
			IF ( reset = '1' ) THEN
				rx_data <= ( others => '0' );
				rx_data_ready <= '0';
				tx_data_done <= '0';
				net_data_outgoing <= ( others => '0' );
			ELSE
				IF ( mode = '0') THEN
					-- RECEIVE
					IF ( net_data_available = '1' ) THEN
						-- RECEIVE THE DATA.
						rx_data <= net_data_incoming;
						-- Set the data as available.
						IF (rx_data_ready = '0') THEN
							rx_data_ready <= '1';
						END IF;
					ELSE
						rx_data       <= ( others => '0' );
						rx_data_ready <= '0';
					END IF;
				ELSE
					-- SEND
					IF tx_data_ready = '1' THEN
						-- Send the data.
						net_data_outgoing <= tx_data;
						tx_data_done <= net_data_sent;
					ELSE
						net_data_outgoing <= ( others => '0' );
						tx_data_done <= '0';
					END IF;
				END IF;
			END IF;
		END IF;
	END PROCESS;
END network_interface;
