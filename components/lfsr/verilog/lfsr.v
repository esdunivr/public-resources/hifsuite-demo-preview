// ----------------------------------------------------------
// Design   : Simple Linear Feedback Shift Register (LFSR)
// Author   : Enrico Fraccaroli
// Modified : 01 Jun 2018
// Description :
// A Linear Feedback Shift Register is a sequential shift register
// with combinational feedback logic around it that causes it to pseudo
// randomly cycle through a sequence of binary values.
// Feedback around LFSR's shift register comes from a selection of points
// in the register chain and constitute either XORing or XNORing these
// points to provide point back into the register.
// The LFSR basically loops through repetitive sequences of pseudo random
// values. The maximum length of sequence is (2^n) - 1.
// ----------------------------------------------------------

module lfsr(clk, rst, ready, out);
	input clk;
	input rst;
	input ready;
	output reg [7:0] out;

	wire linear_feedback;
	
	assign linear_feedback =  ! (out[7] ^ out[3]);

	always @(posedge clk) begin
    	if (rst) begin
    		out   = 8'b0;
			ready = 1'b0;
    	end
    	else begin
			out <= {out[6], out[5], out[4], out[3], out[2],out[1], out[0], linear_feedback};
			ready = 1'b1;
		end
	end
endmodule