// ----------------------------------------------------------
// Design   : Wrapper for a LFSR.
// Author   : Enrico Fraccaroli
// Modified : 01 Jun 2018
// ----------------------------------------------------------

module lfsr_apb_wrapper(
    pclk,
    presetn,
    paddr,
    psel,
    penable,
    pwrite,
    pwdata,
    pready,
    prdata);
    // ------------------------------------------------------------------------
    // PORTS
    // ------------------------------------------------------------------------
    input             pclk;
    input             presetn;
    input [31:0]      paddr;
    input             psel;
    input             penable;
    input             pwrite;
    input [31:0]      pwdata;
    output reg        pready;
    output reg [31:0] prdata;

    // ------------------------------------------------------------------------
    // SUB CIRCUITS
    // ------------------------------------------------------------------------
    lfsr lfsr_0(.clk(pclk), .rst(presetn), .ready(pready), .out(prdata));
endmodule