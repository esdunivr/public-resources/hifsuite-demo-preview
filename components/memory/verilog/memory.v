// -------------------------------------------------------
// Design   : Memory for m6502 with Amba Bus interface
// Author   : Stefano Centomo, Enrico Fraccaroli
// Modified : 14 May 2018
// ---------------------------------------------------------

`timescale 1ns / 100ps

`define STACKSIZE   16'h0400
`define MAIN_START  16'hC001
`define MAIN_SIZE   16'h4000
`define STACK_START_ADDRESS (16'h2000 - `STACKSIZE)
`define RAM_SIZE 32768
`define ROM_SIZE (`MAIN_SIZE)

module memory(
	clk,
	rst,
	addr,
	datai,
	datao,
	irq_n,
	nmi_n,
	sob_n,
	res_n,
	rdy,
	vpab,
	sync,
	we_n,
	oeb,
	// BUS INTERFACE
	psel1,
	psel2,
	psel3,
	psel4,
	psel5,
	psel6,
	psel7,
	psel8,
	penable,
	presetn,
	paddr,
	pwrite,
	pwdata,
	pready,
	prdata);

	// ------------------------------------------------------------------------
	// CPU INTERFACE
	// ------------------------------------------------------------------------
	input   				clk;
	input					rst;
	input  		[15:0] 		addr; 			//  address bus
	output reg  [7:0] 		datai; 			//  data bus
	input 		[7:0] 		datao; 			//  data bus
	input 					oeb; 			//  data bus negtive enable
	output reg  			irq_n; 			//  interrupt request
	output reg  			nmi_n; 			//  Non-makable interrupt request
	output reg  			res_n; 			//  reset
	output reg  			rdy ; 			//  ready
	output reg  			sob_n; 			//  Set Overflow bit
	input  					sync; 			//  indicates an opcode fetch
	input  					vpab; 			//  Vector Pull Address
	input  					we_n; 			//  1=read,0=write

	// ------------------------------------------------------------------------
	// BUS INTERFACE
	// ------------------------------------------------------------------------
	output reg 			presetn;
	output reg 	[31:0] 	paddr;

	output reg 			psel1;
	output reg 			psel2;
	output reg 			psel3;
	output reg 			psel4;
	output reg 			psel5;
	output reg 			psel6;
	output reg 			psel7;
	output reg 			psel8;
	output reg 			penable;
	output reg 			pwrite;
	output reg 	[31:0] 	pwdata;
	input  				pready;
	input  		[31:0] 	prdata;

	// ------------------------------------------------------------------------
	//  MEMORY MAPPED IO
	// ------------------------------------------------------------------------
	localparam PSEL_ADDR	= `STACK_START_ADDRESS + 16'h001F ;

	localparam PWDATA_7_0  	= `STACK_START_ADDRESS + 16'h0020 ;
	localparam PWDATA_15_8 	= `STACK_START_ADDRESS + 16'h0021 ;
	localparam PWDATA_23_16 = `STACK_START_ADDRESS + 16'h0022 ;
	localparam PWDATA_31_24 = `STACK_START_ADDRESS + 16'h0023 ;

	localparam PRDATA_7_0	= `STACK_START_ADDRESS + 16'h0024 ;
	localparam PRDATA_15_8	= `STACK_START_ADDRESS + 16'h0025 ;
	localparam PRDATA_23_16	= `STACK_START_ADDRESS + 16'h0026 ;
	localparam PRDATA_31_24	= `STACK_START_ADDRESS + 16'h0027 ;

	localparam FLAGS		= `STACK_START_ADDRESS + 16'h0028 ;

	localparam PADDR_7_0  	= `STACK_START_ADDRESS + 16'h001B ;
	localparam PADDR_15_8 	= `STACK_START_ADDRESS + 16'h001C ;
	localparam PADDR_23_16 	= `STACK_START_ADDRESS + 16'h001D ;
	localparam PADDR_31_24 	= `STACK_START_ADDRESS + 16'h001E ;

	// ------------------------------------------------------------------------
	//  FSM
	// ------------------------------------------------------------------------
	localparam STATE_SIZE = 4;
	localparam st_start	= 4'b0000;
	localparam st_1		= 4'b0001;
	localparam st_2		= 4'b0010;
	localparam st_3		= 4'b0011;
	localparam st_4		= 4'b0100;
	localparam st_5		= 4'b0101;
	localparam st_6		= 4'b0110;
	localparam st_7		= 4'b0111;
	localparam st_8		= 4'b1000;
	localparam st_9		= 4'b1001;
	reg[STATE_SIZE - 1 : 0] state;

	// ------------------------------------------------------------------------
	// MEMORY ACCESS
	// ------------------------------------------------------------------------
	reg[7:0]  _ram[`RAM_SIZE - 1 : 0];
	reg[7:0]  _rom[`ROM_SIZE - 1 : 0];
	reg[13:0] ram_addr;
	reg[13:0] rom_addr;
	reg addr_firstbit_ram;
	reg addr_firstbit_rom;

	//-----------------------------------------
	// BEGINNING
	//-----------------------------------------
	initial begin
		state 		= 	st_start;
		datai		<= 8'h00;
		irq_n		<= 1'b0;
		nmi_n		<= 1'b0;
		res_n		<= 1'b0;
		rdy			<= 1'b0;
		sob_n		<= 1'b0;

		// Bus interface
		psel1 		<= 1'b0;
		psel2 		<= 1'b0;
		psel3		<= 1'b0;
		psel4		<= 1'b0;
		psel5 		<= 1'b0;
		psel6 		<= 1'b0;
		psel7		<= 1'b0;
		psel8		<= 1'b0;
		penable		<= 1'b0;
		presetn		<= 1'b0;
		pwrite		<= 1'b0;
		paddr 		<= 32'h0000;
		pwdata 		<= 32'h00000000;
		//$readmemh ("../roms/rom.mem", _rom);
	end

	//-----------------------------------------
	// MEMORY DATA PATH
	//-----------------------------------------
	always @(state) begin
		case( state )
			st_start: begin
				rdy			<= 1'b0;
				sob_n		<= 1'b0;
				irq_n		<= 1'b0;
				nmi_n		<= 1'b0;
				res_n		<= 1'b0;
			end
			st_1: begin
				rdy 	<= 1'b1;  // CHANGED
				sob_n	<= 1'b1;  // CHANGED
				irq_n 	<= 1'b0;
				nmi_n 	<= 1'b0;
				res_n 	<= 1'b0;
			end
			st_2: begin
				rdy 	<= 1'b1;
				sob_n	<= 1'b1;
				irq_n 	<= 1'b0;
				nmi_n 	<= 1'b0;
				res_n 	<= 1'b0;
			end
			st_3: begin
				rdy 	<= 1'b1;
				sob_n	<= 1'b1;
				irq_n 	<= 1'b0;
				nmi_n 	<= 1'b1; // CHANGED
				res_n 	<= 1'b0;
			end
			st_4: begin
				rdy 	<= 1'b1;
				sob_n	<= 1'b1;
				irq_n 	<= 1'b0;
				nmi_n 	<= 1'b1;
				res_n 	<= 1'b0;
			end
			st_5: begin
				rdy 	<= 1'b1;
				sob_n	<= 1'b1;
				irq_n 	<= 1'b0;
				nmi_n 	<= 1'b1;
				res_n 	<= 1'b0;
			end
			st_6: begin
				rdy 	<= 1'b1;
				sob_n	<= 1'b1;
				irq_n 	<= 1'b0;
				nmi_n 	<= 1'b0; // CHANGED
				res_n 	<= 1'b0;
			end
			st_7: begin
				rdy 	<= 1'b1;
				sob_n	<= 1'b1;
				irq_n 	<= 1'b0;
				nmi_n 	<= 1'b0;
				res_n 	<= 1'b1; // CHANGED
			end
			st_8: begin
				rdy 	<= 1'b1;
				sob_n	<= 1'b1;
				irq_n 	<= 1'b0;
				nmi_n 	<= 1'b0;
				res_n 	<= 1'b1;
			end
			st_9: begin
				rdy 	<= 1'b1;
				sob_n	<= 1'b1;
				irq_n 	<= 1'b0;
				nmi_n 	<= 1'b0;
				res_n 	<= 1'b1;
			end
		endcase
	end

	//-----------------------------------------
	// MEMORY FSM
	//-----------------------------------------
	always @ ( posedge clk ) begin
		// POSEDGE RESET!
		if ( rst==1'b1 ) begin
			state = st_start;
		end
		else begin
			case( state )
				st_start: begin
					state = st_1;
				end
				st_1: begin
					state = st_2;
				end
				st_2: begin
					state = st_3;
				end
				st_3: begin
					state = st_4;
				end
				st_4: begin
					state = st_5;
				end
				st_5: begin
					state = st_6;
				end
				st_6: begin
					state = st_7;
				end
				st_7: begin
					state = st_8;
				end
				st_8: begin
					state = st_9;
				end
				st_9: begin
					state = st_8;
				end
			endcase
		end
	end

	//-----------------------------------------
	// RAM/ROM READ/WRITE PROCESS
	//-----------------------------------------

	always @( clk or addr ) begin
		ram_addr = addr[13:0];
		rom_addr = addr[13:0];
		addr_firstbit_ram = addr[15];
		addr_firstbit_rom = addr[15];
		if ( ~addr_firstbit_ram & ~we_n )
			_ram[ram_addr] = datao;
		else if ( ~addr_firstbit_ram & we_n)
			datai <= _ram[ram_addr];
		else
		if ( addr_firstbit_rom 	& we_n 	)
			datai <= _rom[rom_addr];
	end

	//-----------------------------------------
	//		BUS INTERACTION
	//-----------------------------------------
	always @(posedge clk) begin
		//Write Phase
		psel1 <= _ram[PSEL_ADDR][0];
		psel2 <= _ram[PSEL_ADDR][1];
		psel3 <= _ram[PSEL_ADDR][2];
		psel4 <= _ram[PSEL_ADDR][3];
		psel5 <= _ram[PSEL_ADDR][4];
		psel6 <= _ram[PSEL_ADDR][5];
		psel7 <= _ram[PSEL_ADDR][6];
		psel8 <= _ram[PSEL_ADDR][7];

		penable <= _ram[FLAGS][0];
		presetn <= _ram[FLAGS][1];
		pwrite  <= _ram[FLAGS][2];

		pwdata[31:24] 		<= _ram[PWDATA_31_24];
		pwdata[23:16] 		<= _ram[PWDATA_23_16];
		pwdata[15:8]		<= _ram[PWDATA_15_8];
		pwdata[7:0]			<= _ram[PWDATA_7_0];

		paddr[31:24] 		<= _ram[PADDR_31_24];
		paddr[23:16] 		<= _ram[PADDR_23_16];
		paddr[15:8]			<= _ram[PADDR_15_8];
		paddr[7:0]			<= _ram[PADDR_7_0];

		//Read Phase
		_ram[PRDATA_31_24] 	= prdata[31:24];
		_ram[PRDATA_23_16] 	= prdata[23:16];
		_ram[PRDATA_15_8] 	= prdata[15:8];
		_ram[PRDATA_7_0] 	= prdata[7:0];

		_ram[FLAGS][3]		= pready;
	end
endmodule
