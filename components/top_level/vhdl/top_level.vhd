
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY complete_platform IS
	PORT (
		clk	: in std_logic;
		rst	: in std_logic;

		--
		-- Sensor Interface
		--
		sensor_high : in std_logic;
		sensor_low  : in std_logic;

		--
		-- Actuator Interface
		--
		actuator_open 		: out std_logic;
		actuator_close 		: out std_logic;
		actuator_threshold 	: out std_logic_vector(15 downto 0)

	);
END;

ARCHITECTURE complete_arch  OF complete_platform IS

-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- COMPONENTS
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

-------------------------------------------------------------------------------
-- CPU (m6502.v)
-------------------------------------------------------------------------------
COMPONENT m6502
	PORT (
		-- from CPU
		clk		: in std_logic;
		addr 	: out std_logic_vector(15 downto 0);
		datai 	: in std_logic_vector(7 downto 0);
		datao 	: out std_logic_vector(7 downto 0);
		oeb		: out std_logic;
		irq_n 	: in std_logic;
		nmi_n 	: in std_logic;
		res_n 	: in std_logic;
		rdy 	: in std_logic;
		sob_n 	: in std_logic;
		sync	: out std_logic;
		vpab	: out std_logic;
		we_n	: out std_logic
	);
END COMPONENT m6502;

-------------------------------------------------------------------------------
-- MEMORIA (memory.v)
-------------------------------------------------------------------------------
COMPONENT memory
	PORT (
		-- from CPU
		clk		: in std_logic;
		rst		: in std_logic;
		addr 	: in std_logic_vector(15 downto 0);
		datai 	: out std_logic_vector(7 downto 0);
		datao 	: in std_logic_vector(7 downto 0);
		irq_n 	: out std_logic;
		nmi_n 	: out std_logic;
		sob_n 	: out std_logic;
		res_n 	: out std_logic;
		rdy 	: out std_logic;
		vpab	: in std_logic;
		sync	: in std_logic;
		we_n	: in std_logic;
		oeb		: in std_logic;
		-- to BUS
		psel1	: out std_logic;
		psel2	: out std_logic; 
		psel3	: out std_logic;
		psel4	: out std_logic;
		psel5	: out std_logic;
		psel6	: out std_logic;
		psel7	: out std_logic;
		psel8	: out std_logic;
		penable	: out std_logic;
		presetn	: out std_logic;
		paddr	: out std_logic_vector(31 downto 0);
		pwrite	: out std_logic;
		pwdata	: out std_logic_vector(31 downto 0);
		pready	: in std_logic;
		prdata  : in std_logic_vector(31 downto 0)
	);
END COMPONENT memory;

-------------------------------------------------------------------------------
-- BUS (amba_apb_bus.vhd)
-------------------------------------------------------------------------------
COMPONENT amba_apb_bus IS
PORT (
    ---------------------------------------------------------------------------
    clk                     : in    std_logic;
    -- MASTER Interface -------------------------------------------------------
    apb_master_pclk         : out   std_logic;
    apb_master_presetn      : in    std_logic; -- to fix! 
    apb_master_paddr        : in    std_logic_vector(31 downto 0);
    apb_master_psel1        : in    std_logic;
    apb_master_psel2        : in    std_logic;
    apb_master_psel3        : in    std_logic;
    apb_master_psel4        : in    std_logic;
    apb_master_psel5        : in    std_logic;
    apb_master_psel6        : in    std_logic;
    apb_master_psel7        : in    std_logic;
    apb_master_psel8        : in    std_logic;
    apb_master_penable      : in    std_logic;
    apb_master_pwrite       : in    std_logic;
    apb_master_pwdata       : in    std_logic_vector(31 downto 0);
    apb_master_pready       : out   std_logic;
    apb_master_prdata       : out   std_logic_vector(31 downto 0);
    -- SLAVE Interface 1 ------------------------------------------------------
    apb_1_pclk              : out   std_logic;
    apb_1_presetn           : out   std_logic;
    apb_1_paddr             : out   std_logic_vector(31 downto 0);
    apb_1_psel              : out   std_logic;
    apb_1_penable           : out   std_logic;
    apb_1_pwrite            : out   std_logic;
    apb_1_pwdata            : out   std_logic_vector(31 downto 0);
    apb_1_pready            : in    std_logic;
    apb_1_prdata            : in    std_logic_vector(31 downto 0);
    -- SLAVE Interface 2 ------------------------------------------------------
    apb_2_pclk              : out   std_logic;
    apb_2_presetn           : out   std_logic;
    apb_2_paddr             : out   std_logic_vector(31 downto 0);
    apb_2_psel              : out   std_logic;
    apb_2_penable           : out   std_logic;
    apb_2_pwrite            : out   std_logic;
    apb_2_pwdata            : out   std_logic_vector(31 downto 0);
    apb_2_pready            : in    std_logic;
    apb_2_prdata            : in    std_logic_vector(31 downto 0);
    -- SLAVE Interface 3 ------------------------------------------------------
    apb_3_pclk              : out   std_logic;
    apb_3_presetn           : out   std_logic;
    apb_3_paddr             : out   std_logic_vector(31 downto 0);
    apb_3_psel              : out   std_logic;
    apb_3_penable           : out   std_logic;
    apb_3_pwrite            : out   std_logic;
    apb_3_pwdata            : out   std_logic_vector(31 downto 0);
    apb_3_pready            : in    std_logic;
    apb_3_prdata            : in    std_logic_vector(31 downto 0);
    -- SLAVE Interface 4 ------------------------------------------------------
    apb_4_pclk              : out   std_logic;
    apb_4_presetn           : out   std_logic;
    apb_4_paddr             : out   std_logic_vector(31 downto 0);
    apb_4_psel              : out   std_logic;
    apb_4_penable           : out   std_logic;
    apb_4_pwrite            : out   std_logic;
    apb_4_pwdata            : out   std_logic_vector(31 downto 0);
    apb_4_pready            : in    std_logic;
    apb_4_prdata            : in    std_logic_vector(31 downto 0);
    -- SLAVE Interface 5 ------------------------------------------------------
    apb_5_pclk              : out   std_logic;
    apb_5_presetn           : out   std_logic;
    apb_5_paddr             : out   std_logic_vector(31 downto 0);
    apb_5_psel              : out   std_logic;
    apb_5_penable           : out   std_logic;
    apb_5_pwrite            : out   std_logic;
    apb_5_pwdata            : out   std_logic_vector(31 downto 0);
    apb_5_pready            : in    std_logic;
    apb_5_prdata            : in    std_logic_vector(31 downto 0);
    -- SLAVE Interface 6 ------------------------------------------------------
    apb_6_pclk              : out   std_logic;
    apb_6_presetn           : out   std_logic;
    apb_6_paddr             : out   std_logic_vector(31 downto 0);
    apb_6_psel              : out   std_logic;
    apb_6_penable           : out   std_logic;
    apb_6_pwrite            : out   std_logic;
    apb_6_pwdata            : out   std_logic_vector(31 downto 0);
    apb_6_pready            : in    std_logic;
    apb_6_prdata            : in    std_logic_vector(31 downto 0);
    -- SLAVE Interface 7 ------------------------------------------------------
    apb_7_pclk              : out   std_logic;
    apb_7_presetn           : out   std_logic;
    apb_7_paddr             : out   std_logic_vector(31 downto 0);
    apb_7_psel              : out   std_logic;
    apb_7_penable           : out   std_logic;
    apb_7_pwrite            : out   std_logic;
    apb_7_pwdata            : out   std_logic_vector(31 downto 0);
    apb_7_pready            : in    std_logic;
    apb_7_prdata            : in    std_logic_vector(31 downto 0);
    -- SLAVE Interface 8 ------------------------------------------------------
    apb_8_pclk              : out   std_logic;
    apb_8_presetn           : out   std_logic;
    apb_8_paddr             : out   std_logic_vector(31 downto 0);
    apb_8_psel              : out   std_logic;
    apb_8_penable           : out   std_logic;
    apb_8_pwrite            : out   std_logic;
    apb_8_pwdata            : out   std_logic_vector(31 downto 0);
    apb_8_pready            : in    std_logic;
    apb_8_prdata            : in    std_logic_vector(31 downto 0)
    ---------------------------------------------------------------------------
);
END COMPONENT amba_apb_bus;

-------------------------------------------------------------------------------
-- MULTIPLIER (multiplier.vhd)
-------------------------------------------------------------------------------
COMPONENT multiplier_apb_wrapper
	PORT (
		pclk	: in std_logic;
		presetn : in std_logic;
		paddr 	: in std_logic_vector(31 downto 0);
		psel 	: in std_logic;
		penable : in std_logic;
		pwrite 	: in std_logic;
		pwdata 	: in std_logic_vector(31 downto 0);
		pready 	: out std_logic;
		prdata 	: out std_logic_vector(31 downto 0)
	);
END COMPONENT multiplier_apb_wrapper;

-------------------------------------------------------------------------------
-- ACCELEROMETER (accelerometer_apb_wrapper.va)
-------------------------------------------------------------------------------
COMPONENT accelerometer_apb_wrapper
	PORT (
		pclk	     : in std_logic;
		presetn      : in std_logic;
		paddr 	     : in std_logic_vector(31 downto 0);
		psel 	     : in std_logic;
		penable      : in std_logic;
		pwrite 	     : in std_logic;
		pwdata 	     : in std_logic_vector(31 downto 0);
		pready 	     : out std_logic;
		prdata 	     : out std_logic_vector(31 downto 0);
		analog_clock : in std_logic
	);
END COMPONENT accelerometer_apb_wrapper;

-------------------------------------------------------------------------------
-- NETWORK INTERFACE (network_interface_apb_wrapper.vhd)
-------------------------------------------------------------------------------
COMPONENT network_interface_apb_wrapper
	PORT (
		pclk	: in std_logic;
		presetn : in std_logic;
		paddr 	: in std_logic_vector(31 downto 0);
		psel 	: in std_logic;
		penable : in std_logic;
		pwrite 	: in std_logic;
		pwdata 	: in std_logic_vector(31 downto 0);
		pready 	: out std_logic;
		prdata 	: out std_logic_vector(31 downto 0)
	);
END COMPONENT network_interface_apb_wrapper;

COMPONENT sensor
	PORT (
		pclk			: in std_logic;
		presetn 		: in std_logic;
		paddr 			: in std_logic_vector(31 downto 0);
		psel 			: in std_logic;
		penable 		: in std_logic;
		pwrite 			: in std_logic;
		pwdata 			: in std_logic_vector(31 downto 0);
		pready 			: out std_logic;
		prdata 			: out std_logic_vector(31 downto 0);
		sensor_high 	: in std_logic;
		sensor_low 		: in std_logic
	);
END COMPONENT sensor;

COMPONENT actuator
	PORT (
		pclk				: in std_logic;
		presetn 			: in std_logic;
		paddr 				: in std_logic_vector(31 downto 0);
		psel 				: in std_logic;
		penable 			: in std_logic;
		pwrite 				: in std_logic;
		pwdata 				: in std_logic_vector(31 downto 0);
		pready 				: out std_logic;
		prdata 				: out std_logic_vector(31 downto 0);
		actuator_open		: out std_logic; 
		actuator_close		: out std_logic; 
		actuator_threshold	: out std_logic_vector(15 downto 0)
	);
END COMPONENT actuator;

COMPONENT lfsr_apb_wrapper
	PORT (
		pclk				: in std_logic;
		presetn 			: in std_logic;
		paddr 				: in std_logic_vector(31 downto 0);
		psel 				: in std_logic;
		penable 			: in std_logic;
		pwrite 				: in std_logic;
		pwdata 				: in std_logic_vector(31 downto 0);
		pready 				: out std_logic;
		prdata 				: out std_logic_vector(31 downto 0)
	);
END COMPONENT lfsr_apb_wrapper;

COMPONENT watchdog_apb_wrapper
	PORT (
		pclk				: in std_logic;
		presetn 			: in std_logic;
		paddr 				: in std_logic_vector(31 downto 0);
		psel 				: in std_logic;
		penable 			: in std_logic;
		pwrite 				: in std_logic;
		pwdata 				: in std_logic_vector(31 downto 0);
		pready 				: out std_logic;
		prdata 				: out std_logic_vector(31 downto 0)
	);
END COMPONENT watchdog;

-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- SIGNALS
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- Signal for MASTER
SIGNAL apb_master_pclk         : std_logic;
SIGNAL apb_master_presetn      : std_logic;
SIGNAL apb_master_paddr        : std_logic_vector(31 downto 0);
SIGNAL apb_master_psel1        : std_logic;
SIGNAL apb_master_psel2        : std_logic;
SIGNAL apb_master_psel3        : std_logic;
SIGNAL apb_master_psel4        : std_logic;
SIGNAL apb_master_psel5        : std_logic;
SIGNAL apb_master_psel6        : std_logic;
SIGNAL apb_master_psel7        : std_logic;
SIGNAL apb_master_psel8        : std_logic;
SIGNAL apb_master_penable      : std_logic;
SIGNAL apb_master_pwrite       : std_logic;
SIGNAL apb_master_pwdata       : std_logic_vector(31 downto 0);
SIGNAL apb_master_pready       : std_logic;
SIGNAL apb_master_prdata       : std_logic_vector(31 downto 0);
-- Signal for SLAVE 1
SIGNAL apb_1_pclk              : std_logic;
SIGNAL apb_1_presetn           : std_logic;
SIGNAL apb_1_paddr             : std_logic_vector(31 downto 0);
SIGNAL apb_1_psel              : std_logic;
SIGNAL apb_1_penable           : std_logic;
SIGNAL apb_1_pwrite            : std_logic;
SIGNAL apb_1_pwdata            : std_logic_vector(31 downto 0);
SIGNAL apb_1_pready            : std_logic;
SIGNAL apb_1_prdata            : std_logic_vector(31 downto 0);
-- Signal for SLAVE 2
SIGNAL apb_2_pclk              : std_logic;
SIGNAL apb_2_presetn           : std_logic;
SIGNAL apb_2_paddr             : std_logic_vector(31 downto 0);
SIGNAL apb_2_psel              : std_logic;
SIGNAL apb_2_penable           : std_logic;
SIGNAL apb_2_pwrite            : std_logic;
SIGNAL apb_2_pwdata            : std_logic_vector(31 downto 0);
SIGNAL apb_2_pready            : std_logic;
SIGNAL apb_2_prdata            : std_logic_vector(31 downto 0);
-- Signal for SLAVE 3
SIGNAL apb_3_pclk              : std_logic;
SIGNAL apb_3_presetn           : std_logic;
SIGNAL apb_3_paddr             : std_logic_vector(31 downto 0);
SIGNAL apb_3_psel              : std_logic;
SIGNAL apb_3_penable           : std_logic;
SIGNAL apb_3_pwrite            : std_logic;
SIGNAL apb_3_pwdata            : std_logic_vector(31 downto 0);
SIGNAL apb_3_pready            : std_logic;
SIGNAL apb_3_prdata            : std_logic_vector(31 downto 0);
-- Signal for SLAVE 4
SIGNAL apb_4_pclk              : std_logic;
SIGNAL apb_4_presetn           : std_logic;
SIGNAL apb_4_paddr             : std_logic_vector(31 downto 0);
SIGNAL apb_4_psel              : std_logic;
SIGNAL apb_4_penable           : std_logic;
SIGNAL apb_4_pwrite            : std_logic;
SIGNAL apb_4_pwdata            : std_logic_vector(31 downto 0);
SIGNAL apb_4_pready            : std_logic;
SIGNAL apb_4_prdata            : std_logic_vector(31 downto 0);
-- Signal for SLAVE 5
SIGNAL apb_5_pclk              : std_logic;
SIGNAL apb_5_presetn           : std_logic;
SIGNAL apb_5_paddr             : std_logic_vector(31 downto 0);
SIGNAL apb_5_psel              : std_logic;
SIGNAL apb_5_penable           : std_logic;
SIGNAL apb_5_pwrite            : std_logic;
SIGNAL apb_5_pwdata            : std_logic_vector(31 downto 0);
SIGNAL apb_5_pready            : std_logic;
SIGNAL apb_5_prdata            : std_logic_vector(31 downto 0);
-- Signal for SLAVE 6
SIGNAL apb_6_pclk              : std_logic;
SIGNAL apb_6_presetn           : std_logic;
SIGNAL apb_6_paddr             : std_logic_vector(31 downto 0);
SIGNAL apb_6_psel              : std_logic;
SIGNAL apb_6_penable           : std_logic;
SIGNAL apb_6_pwrite            : std_logic;
SIGNAL apb_6_pwdata            : std_logic_vector(31 downto 0);
SIGNAL apb_6_pready            : std_logic;
SIGNAL apb_6_prdata            : std_logic_vector(31 downto 0);
-- Signal for SLAVE 7
SIGNAL apb_7_pclk              : std_logic;
SIGNAL apb_7_presetn           : std_logic;
SIGNAL apb_7_paddr             : std_logic_vector(31 downto 0);
SIGNAL apb_7_psel              : std_logic;
SIGNAL apb_7_penable           : std_logic;
SIGNAL apb_7_pwrite            : std_logic;
SIGNAL apb_7_pwdata            : std_logic_vector(31 downto 0);
SIGNAL apb_7_pready            : std_logic;
SIGNAL apb_7_prdata            : std_logic_vector(31 downto 0);
-- Signal for SLAVE 8
SIGNAL apb_8_pclk              : std_logic;
SIGNAL apb_8_presetn           : std_logic;
SIGNAL apb_8_paddr             : std_logic_vector(31 downto 0);
SIGNAL apb_8_psel              : std_logic;
SIGNAL apb_8_penable           : std_logic;
SIGNAL apb_8_pwrite            : std_logic;
SIGNAL apb_8_pwdata            : std_logic_vector(31 downto 0);
SIGNAL apb_8_pready            : std_logic;
SIGNAL apb_8_prdata            : std_logic_vector(31 downto 0);

-- Signals for CPU
--SIGNAL clk					   : std_logic;
SIGNAL addr 				   : std_logic_vector(15 downto 0);
SIGNAL datai 				   : std_logic_vector(7 downto 0);
SIGNAL datao 				   : std_logic_vector(7 downto 0);
SIGNAL oeb					   : std_logic;
SIGNAL irq_n 				   : std_logic;
SIGNAL nmi_n 				   : std_logic;
SIGNAL res_n 				   : std_logic;
SIGNAL rdy 					   : std_logic;
SIGNAL sob_n 				   : std_logic;
SIGNAL sync					   : std_logic;
SIGNAL vpab					   : std_logic;
SIGNAL we_n					   : std_logic;

BEGIN

-- ------------------------------------------------------------------------
-- CPU
-- ------------------------------------------------------------------------
m6502_0 :  m6502
PORT MAP(
	clk		=> clk,
	addr 	=> addr,
	datai 	=> datai,
	datao 	=> datao,
	oeb		=> oeb,
	irq_n 	=> irq_n,
	nmi_n 	=> nmi_n,
	res_n 	=> res_n,
	rdy 	=> rdy,
	sob_n 	=> sob_n,
	sync	=> sync,
	vpab	=> vpab,
	we_n	=> we_n
);

-- ------------------------------------------------------------------------
-- MEMORY
-- ------------------------------------------------------------------------
memory_0 : memory
PORT MAP(
	-- Interface to CPU
	clk		=> clk,
	rst		=> rst,
	addr 	=> addr,
	datai 	=> datai,
	datao 	=> datao,
	irq_n 	=> irq_n,
	nmi_n 	=> nmi_n,
	sob_n 	=> sob_n,
	res_n 	=> res_n,
	rdy 	=> rdy,
	vpab	=> vpab,
	sync	=> sync,
	we_n	=> we_n,
	oeb		=> oeb,
	-- Interface to BUS
	psel1	=> apb_master_psel1,
	psel2	=> apb_master_psel2, 
	psel3	=> apb_master_psel3,
	psel4	=> apb_master_psel4,
	psel5	=> apb_master_psel5,
	psel6	=> apb_master_psel6,
	psel7	=> apb_master_psel7,
	psel8	=> apb_master_psel8,
	penable	=> apb_master_penable,
	presetn	=> apb_master_presetn,
	paddr	=> apb_master_paddr,
	pwrite	=> apb_master_pwrite,
	pwdata	=> apb_master_pwdata,
	pready	=> apb_master_pready,
	prdata  => apb_master_prdata
);

-- ------------------------------------------------------------------------
-- amba_apb_bus
-- ------------------------------------------------------------------------
amba_apb_bus_0 : amba_apb_bus
PORT MAP(
	clk                 => clk,
    -- MASTER Interface -------------------------------------------------------
    apb_master_pclk     => apb_master_pclk,
    apb_master_presetn  => apb_master_presetn,
    apb_master_paddr    => apb_master_paddr,
    apb_master_psel1    => apb_master_psel1,
    apb_master_psel2    => apb_master_psel2,
    apb_master_psel3    => apb_master_psel3,
    apb_master_psel4    => apb_master_psel4,
    apb_master_psel5    => apb_master_psel5,
    apb_master_psel6    => apb_master_psel6,
    apb_master_psel7    => apb_master_psel7,
    apb_master_psel8    => apb_master_psel8,
    apb_master_penable  => apb_master_penable,
    apb_master_pwrite   => apb_master_pwrite,
    apb_master_pwdata   => apb_master_pwdata,
    apb_master_pready   => apb_master_pready,
    apb_master_prdata	=> apb_master_prdata,
    -- SLAVE Interface 1 ------------------------------------------------------
	apb_1_pclk      	=> apb_1_pclk,
    apb_1_presetn   	=> apb_1_presetn,
    apb_1_paddr     	=> apb_1_paddr,
    apb_1_psel      	=> apb_1_psel,
    apb_1_penable   	=> apb_1_penable,
    apb_1_pwrite    	=> apb_1_pwrite,
    apb_1_pwdata    	=> apb_1_pwdata,
    apb_1_pready    	=> apb_1_pready,
    apb_1_prdata		=> apb_1_prdata,
    -- SLAVE Interface 2 ------------------------------------------------------
    apb_2_pclk      	=> apb_2_pclk,
    apb_2_presetn   	=> apb_2_presetn,
    apb_2_paddr     	=> apb_2_paddr,
    apb_2_psel      	=> apb_2_psel,
    apb_2_penable   	=> apb_2_penable,
    apb_2_pwrite    	=> apb_2_pwrite,
    apb_2_pwdata    	=> apb_2_pwdata,
    apb_2_pready    	=> apb_2_pready,
    apb_2_prdata		=> apb_2_prdata,
    -- SLAVE Interface 3 ------------------------------------------------------
    apb_3_pclk      	=> apb_3_pclk,
    apb_3_presetn   	=> apb_3_presetn,
    apb_3_paddr     	=> apb_3_paddr,
    apb_3_psel      	=> apb_3_psel,
    apb_3_penable   	=> apb_3_penable,
    apb_3_pwrite    	=> apb_3_pwrite,
    apb_3_pwdata    	=> apb_3_pwdata,
    apb_3_pready    	=> apb_3_pready,
    apb_3_prdata		=> apb_3_prdata,
    -- SLAVE Interface 4 ------------------------------------------------------
    apb_4_pclk      	=> apb_4_pclk,
    apb_4_presetn   	=> apb_4_presetn,
    apb_4_paddr     	=> apb_4_paddr,
    apb_4_psel      	=> apb_4_psel,
    apb_4_penable   	=> apb_4_penable,
    apb_4_pwrite    	=> apb_4_pwrite,
    apb_4_pwdata    	=> apb_4_pwdata,
    apb_4_pready    	=> apb_4_pready,
    apb_4_prdata		=> apb_4_prdata,
    -- SLAVE Interface 5 ------------------------------------------------------
    apb_5_pclk      	=> apb_5_pclk,
    apb_5_presetn   	=> apb_5_presetn,
    apb_5_paddr     	=> apb_5_paddr,
    apb_5_psel      	=> apb_5_psel,
    apb_5_penable   	=> apb_5_penable,
    apb_5_pwrite    	=> apb_5_pwrite,
    apb_5_pwdata    	=> apb_5_pwdata,
    apb_5_pready    	=> apb_5_pready,
    apb_5_prdata		=> apb_5_prdata,
    -- SLAVE Interface 6 ------------------------------------------------------
    apb_6_pclk      	=> apb_6_pclk,
    apb_6_presetn   	=> apb_6_presetn,
    apb_6_paddr     	=> apb_6_paddr,
    apb_6_psel      	=> apb_6_psel,
    apb_6_penable   	=> apb_6_penable,
    apb_6_pwrite    	=> apb_6_pwrite,
    apb_6_pwdata    	=> apb_6_pwdata,
    apb_6_pready    	=> apb_6_pready,
    apb_6_prdata		=> apb_6_prdata,
    -- SLAVE Interface 7 ------------------------------------------------------
    apb_7_pclk      	=> apb_7_pclk,
    apb_7_presetn   	=> apb_7_presetn,
    apb_7_paddr     	=> apb_7_paddr,
    apb_7_psel      	=> apb_7_psel,
    apb_7_penable   	=> apb_7_penable,
    apb_7_pwrite    	=> apb_7_pwrite,
    apb_7_pwdata    	=> apb_7_pwdata,
    apb_7_pready    	=> apb_7_pready,
    apb_7_prdata		=> apb_7_prdata,
    -- SLAVE Interface 8 ------------------------------------------------------
    apb_8_pclk      	=> apb_8_pclk,
    apb_8_presetn   	=> apb_8_presetn,
    apb_8_paddr     	=> apb_8_paddr,
    apb_8_psel      	=> apb_8_psel,
    apb_8_penable   	=> apb_8_penable,
    apb_8_pwrite    	=> apb_8_pwrite,
    apb_8_pwdata    	=> apb_8_pwdata,
    apb_8_pready    	=> apb_8_pready,
    apb_8_prdata		=> apb_8_prdata
);

-- ------------------------------------------------------------------------
-- MULTIPLIER
-- ------------------------------------------------------------------------
multiplier_0 : multiplier_apb_wrapper
PORT MAP(
	pclk	=> apb_1_pclk,
	presetn => apb_1_presetn,
	paddr 	=> apb_1_paddr,
	psel 	=> apb_1_psel,
	penable => apb_1_penable,
	pwrite 	=> apb_1_pwrite,
	pwdata 	=> apb_1_pwdata,
	pready 	=> apb_1_pready,
	prdata 	=> apb_1_prdata
);

-- ------------------------------------------------------------------------
-- ACCELEROMETER
-- ------------------------------------------------------------------------
accelerometer_0 : accelerometer_apb_wrapper
PORT MAP(
	analog_clock => apb_2_pclk,
	pclk	     => apb_2_pclk,
	presetn      => apb_2_presetn,
	paddr 	     => apb_2_paddr,
	psel 	     => apb_2_psel,
	penable      => apb_2_penable,
	pwrite 	     => apb_2_pwrite,
	pwdata 	     => apb_2_pwdata,
	pready 	     => apb_2_pready,
	prdata 	     => apb_2_prdata
);

-- ------------------------------------------------------------------------
-- NETWORK INTERFACE
-- ------------------------------------------------------------------------
network_interface_0 : network_interface_apb_wrapper
PORT MAP(
	pclk	=> apb_3_pclk,
	presetn => apb_3_presetn,
	paddr 	=> apb_3_paddr,
	psel 	=> apb_3_psel,
	penable => apb_3_penable,
	pwrite 	=> apb_3_pwrite,
	pwdata 	=> apb_3_pwdata,
	pready 	=> apb_3_pready,
	prdata 	=> apb_3_prdata
);

-- ------------------------------------------------------------------------
-- LSFR INTERFACE
-- ------------------------------------------------------------------------
lfsr_0 : lfsr_apb_wrapper 
PORT MAP(
	pclk	=> apb_4_pclk,
	presetn => apb_4_presetn,
	paddr 	=> apb_4_paddr,
	psel 	=> apb_4_psel,
	penable => apb_4_penable,
	pwrite 	=> apb_4_pwrite,
	pwdata 	=> apb_4_pwdata,
	pready 	=> apb_4_pready,
	prdata 	=> apb_4_prdata
); 

-- ------------------------------------------------------------------------
-- SENSOR from WaterTank 
-- ------------------------------------------------------------------------
sensor_0 : sensor
PORT MAP(
	pclk	=> apb_5_pclk,
	presetn => apb_5_presetn,
	paddr 	=> apb_5_paddr,
	psel 	=> apb_5_psel,
	penable => apb_5_penable,
	pwrite 	=> apb_5_pwrite,
	pwdata 	=> apb_5_pwdata,
	pready 	=> apb_5_pready,
	prdata 	=> apb_5_prdata,
	sensor_high => sensor_high,
	sensor_low => sensor_low
);

-- ------------------------------------------------------------------------
-- ACTUATOR to WaterTank
-- ------------------------------------------------------------------------
actuator_0 : actuator
PORT MAP(
	pclk	=> apb_6_pclk,
	presetn => apb_6_presetn,
	paddr 	=> apb_6_paddr,
	psel 	=> apb_6_psel,
	penable => apb_6_penable,
	pwrite 	=> apb_6_pwrite,
	pwdata 	=> apb_6_pwdata,
	pready 	=> apb_6_pready,
	prdata 	=> apb_6_prdata,
	actuator_open => actuator_open,
	actuator_close => actuator_close,
	actuator_threshold => actuator_threshold
); 

-- ------------------------------------------------------------------------
-- WATCHDOG INTERFACE
-- ------------------------------------------------------------------------
watchdog_0 : watchdog_apb_wrapper 
PORT MAP(
	pclk	=> apb_7_pclk,
	presetn => apb_7_presetn,
	paddr 	=> apb_7_paddr,
	psel 	=> apb_7_psel,
	penable => apb_7_penable,
	pwrite 	=> apb_7_pwrite,
	pwdata 	=> apb_7_pwdata,
	pready 	=> apb_7_pready,
	prdata 	=> apb_7_prdata
); 

END complete_arch;
